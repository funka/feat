# README #

### What is this repository for? ###
Feat - FEatures for Accessibility through Templates in EPiServer

### How do I get set up? ###
Clone
Generate EPiServer Demo License - https://license.episerver.com/public/GenerateDemoLicense.aspx
Run in Visual Studio 2019 (or later) or in IIS

### Who do I talk to? ###
info@funka.com

### Environment ####
•	.NET Framework (4.7.2)
•	Microsoft.AspNet.Mvc (5.2.7)
•	Microsoft.AspNet.Razor (3.2.7)
•	EPiServer
o	CMS (11.20)
o	Framework (11.20)
o	EPiServer.Forms (4.30.2)
o	EPiServer.CMS.TinyMce (2.13)
•	ImageResizer (4.2.8)
•	jQuery
o	Main (3.3.1)
o	Validate (1.19)
o	Ajax-unobtrusive (3.2.6)
o	validation-unobtrusive (3.2.11)
o	jquery.form (4.2.2)

### Login ####
Username: Feat
Password: Feat123!
URL: /GUI
