﻿var FunkaEpiForms = FunkaEpiForms || {};
var EpiFormsExtended = EpiFormsExtended || {};

var FunkaEpiForms = {
    onBlur: false,
    Init: function () {
        $.extend(true, epi.EPiServer.Forms, FunkaEpiForms.GetCustomValidators());

        $$epiforms(".EPiServerForms").on("formsStepValidating",
            function (event, param1, param2) {
                FunkaEpiForms.ViewSummryList($(this));
            });

        $("body").on("change",
            ".EPiServerForms .ValidationRequired input[type='checkbox'], .EPiServerForms .ValidationFail input[type='radio'], .EPiServerForms.ValidationFail select",
            function () {
                FunkaEpiForms.onBlur = true;
                FunkaEpiForms.Validate($(this));
            });

        $("body").on("blur",
            ".EPiServerForms.ValidationSuccess input[type='text'], .EPiServerForms.ValidationSuccess textarea",
            function () {
                FunkaEpiForms.onBlur = true;
                if ($(this).val() || $(this).hasClass("ValidationStart")) {
                    FunkaEpiForms.Validate($(this));
                }
            });

        $("body").on("blur",
            ".EPiServerForms.ValidationFail input[type='text'], .EPiServerForms.ValidationFail textarea",
            function () {
                FunkaEpiForms.onBlur = true;
                FunkaEpiForms.Validate($(this));
            });

        var $formState = $("body").find(".Form__Status");

        if ($formState.children().not(".hide").length === 0) {
            $formState.hide();
        }

        $formState.append('<div class="validation-summary-valid" data-valmsg-summary="true"><h3 tabindex="0">The form contains <span data-valmsg-summary-erroramount="true" class=""></span> errors</h3><ul><li style="display: none"></li></ul></div>');

        if ($(".Form__Element.FormFileUpload").length > 0) {
            $.each($(".Form__Element.FormFileUpload"), function () {
                var element = $(this).find(".Form__Element__ValidationError").detach();
                $(this).find("label").append(element);

                $(this).data("label", $(this).find("label").text());
            });
        }
    },
    ViewSummryList: function (form) {
        var container = form.find("[data-valmsg-summary=true]"),
            errorAmount = form.find("[data-valmsg-summary-erroramount]"),
            list = container.find("ul"),
            errorList = FunkaEpiForms.GetElementErrorList(form.find("div.ValidationFail"));

        if (list && list.length && errorList.length) {
            list.empty();
            container.addClass("validation-summary-errors").removeClass("validation-summary-valid");

            $.each(errorList,
                function () {
                    $("<li />").html($(this).data("label")).appendTo(list);
                });

            errorAmount.html(errorList.length);
            container.parent().show();

            if (!FunkaEpiForms.onBlur) {
                window.setTimeout(function () {
                    $(".validation-summary-errors").find("h3").focus();

                    var offset = $(".validation-summary-errors").find("h3").offset();
                    offset.top -= 40;

                    $('html, body').animate({
                        scrollTop: offset.top,
                        scrollLeft: offset.left
                    });
                }, 10);
            }

            FunkaEpiForms.onBlur = false;
        } else {
            container.parent().hide();
        }
    },
    Validate: function ($this) {
        $this.addClass("ValidationStart");

        if ($this.hasClass("FormChoice__Input")) {
            $this = $this.closest(".FormChoice");
        }

        var _utilsSvc = epi.EPiServer.Forms.Utils;
        var $workingForm = _utilsSvc.getWorkingFormFromInnerElement($this);
        var workingFormGuid = _utilsSvc.getFormIdentifier($workingForm);
        var workingFormInfo = epi.EPiServer.Forms[workingFormGuid];
        workingFormInfo.$workingForm = $workingForm;

        var isValid = EpiFormsExtended.ValidateElement($this, workingFormInfo);

        _utilsSvc.raiseFormsEvent(workingFormInfo, { type: "formsStepValidating", isValid: isValid });

        return isValid;
    },
    GetElementErrorList: function (errorList) {
        var outList = [];

        for (var i = 0, l = errorList.length; i < l; i++) {
            if ($(errorList[i]).find(".ValidationSuccess").length <= 0) {
                outList.push($(errorList[i]));
            }
        }

        return outList;
    },
    GetCustomValidators: function () {
        return {
            Validators: {
                //For regular expression validators
                "FunkaLib.Forms.Validators.FurbRequiredValidator": function (fieldName, fieldValue, validatorMetaData) {
                    if (validatorMetaData) {
                        if (Array.isArray(fieldValue)) {
                            if (fieldValue[0] === null || fieldValue.length === 0) {
                                return {
                                    isValid: false,
                                    message: validatorMetaData.model.message
                                };
                            }
                        } else {
                            if (fieldValue === "" || fieldValue && !fieldValue.length /*for FileUpload element*/) {
                                return {
                                    isValid: false,
                                    message: validatorMetaData.model.message
                                };
                            }
                        }
                    }

                    return {
                        isValid: true
                    };
                },
                "FunkaLib.Forms.Validators.FurbEmailValidator": function (fieldName, fieldValue, validatorMetaData) {
                    var validEmail = /^$|[a-zA-Z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&\'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/.test(fieldValue);
                    if (validatorMetaData) {
                        if (!validEmail || /*for FileUpload element*/ (fieldValue && !fieldValue.length)) {
                            return {
                                isValid: false,
                                message: validatorMetaData.model.message
                            };
                        }
                    }

                    return {
                        isValid: true
                    };
                },
                "FunkaLib.Forms.Validators.FurbSwedishZipCodeValidator": function (fieldName, fieldValue, validatorMetaData) {
                    var validZip = /^$|(\d{5})|(\d{3} \d{2})/.test(fieldValue);
                    if (validatorMetaData) {
                        if (!validZip || /*for FileUpload element*/ (fieldValue && !fieldValue.length)) {
                            return {
                                isValid: false,
                                message: validatorMetaData.model.message
                            };
                        }
                    }

                    return {
                        isValid: true
                    };
                },
                "FunkaLib.Forms.Validators.FurbSwedishSocialSecurityNumberValidator": function (fieldName, fieldValue, validatorMetaData) {
                    var validSocialSecurityNumber = /^$|(^(19|20)?(\d{2})(\d{2})(\d{2})([-|+]{0,1})?(\d{3})(\d{0,1})$)/.test(fieldValue);
                    if (validatorMetaData) {
                        if (!validSocialSecurityNumber || /*for FileUpload element*/ (fieldValue && !fieldValue.length)) {
                            return {
                                isValid: false,
                                message: validatorMetaData.model.message
                            };
                        }
                    }

                    return {
                        isValid: true
                    };
                },
                "FunkaLib.Forms.Validators.FurbMaxCharValidator": function (fieldName, fieldValue, validatorMetaData) {
                    var fieldIsValid = fieldValue === "";
                    if (!fieldIsValid) {
                        var charArray = fieldValue.split('');
                        fieldIsValid = validatorMetaData.model.additionalAttributes === null || charArray.length <= parseInt(validatorMetaData.model.additionalAttributes.number);
                    }

                    if (validatorMetaData) {
                        if (!fieldIsValid) {
                            return {
                                isValid: false,
                                message: validatorMetaData.model.message + " " + validatorMetaData.model.additionalAttributes.number + " " + validatorMetaData.model.additionalAttributes.chars
                            };
                        }
                    }

                    return {
                        isValid: true
                    };
                }
            }
        };
    }
};

//Extend from 4.25.1 -> 4.29.3 ! Don't change!
var EpiFormsExtended = {
    ValidateElement: function (element, workingFormInfo) {
        var _utilsSvc = epi.EPiServer.Forms.Utils,
            _validationSvc = epi.EPiServer.Forms.Validation;

        var $element = $(element),
            $messageContainer = EpiFormsExtended.GetValidationMessage($element),
            elementName = _utilsSvc.getElementName($element),
            elementIdentifier = $element.attr("id") || elementName,
            validators = _validationSvc.getElementValidators(workingFormInfo.ValidationInfo, elementIdentifier),
            isValid = true;

        $element.removeClass("ValidationFail ValidationSuccess");
        $element.closest(".Form__Element").removeClass("ValidationFail ValidationSuccess");

        $messageContainer.hide();

        var elementValue = _utilsSvc.getElementValue($element);

        if (workingFormInfo.DependencyInactiveElements.indexOf(elementName) < 0 && validators instanceof Array && validators.length > 0) {
            var results = _validationSvc.validateFormValue(elementName, elementValue, validators);

            var invalidResultItems = $.grep(results,
                function (item) {
                    return item.isValid === false;
                });

            if (invalidResultItems && invalidResultItems.length > 0) {
                var messages = $.map(invalidResultItems,
                    function (item) {
                        return item.message;
                    });

                $element.addClass("ValidationFail");
                $element.closest(".Form__Element").addClass("ValidationFail");
                $messageContainer.text(messages.join(" ")).show();

                isValid = false;
            } else {
                $element.addClass("ValidationSuccess");
                $element.closest(".Form__Element").addClass("ValidationSuccess");
                isValid = true;
            }
        }

        _utilsSvc.raiseFormsEvent(workingFormInfo,
            {
                type: "elementValidated",
                isValid: isValid,

                elementName: elementName,
                elementValue: elementValue,
                elementIdentifier: elementIdentifier,
                element: element,
                $messageContainer: $messageContainer
            });

        return isValid;
    },
    GetValidationMessage: function (/*Object*/$element) {
        var _utilsSvc = epi.EPiServer.Forms.Utils,
            elementName = _utilsSvc.getElementName($element);
        var selector1 = _utilsSvc.stringFormat(
            "{0}[data-epiforms-linked-name='{1}'], {0}[data-epiforms-linked-name='{2}']",
            [".Form__Element__ValidationError", elementName, $element.attr("id")]);
        var selector2 = _utilsSvc.stringFormat("{0}[data-f-linked-name='{1}'], {0}[data-f-linked-name='{2}']",
            ["[data-f-validationerror]", elementName, $element.attr("id")]);
        return $(selector1).add(selector2);
    }
};

(function ($) {
    if (typeof epi === 'undefined' || typeof epi.EPiServer === 'undefined' ||
        typeof epi.EPiServer.Forms === 'undefined') {
        console.error('Forms is not initialized correctly');
        return;
    }

    if (typeof $ === 'undefined') {
        console.error('Forms cannot work without jQuery');
        return;
    }

    FunkaEpiForms.Init();
})($$epiforms || $);