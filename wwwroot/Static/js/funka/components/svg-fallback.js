﻿$(document).ready(function () {

    if (!Modernizr.svg) {
        $('img').each(function () {
            var src = $(this).attr('src');

            var queryStringStartIndex = src.indexOf('?');
            if (queryStringStartIndex !== -1) {

                src = src.substring(0, queryStringStartIndex);
            }

            if (src.toLowerCase().indexOf(".svg") >= 0) {
                $(this).attr('src', $(this).attr('src').replace('.svg', '.png'));
            }
        });

    }

});

