﻿(function ($, window, document) {
    // DOM ready
    $(function () {
        $teasers = $('.contentArea').find('.teaser');
        for (var i = 0; i < $teasers.length; i += 5) {
            var currentChunk = $teasers.slice(i, i + 5);
            $(currentChunk[0]).addClass('yellow-border');
            $(currentChunk[1]).addClass('green-border');
            $(currentChunk[2]).addClass('pink-border');
            $(currentChunk[3]).addClass('blue-border');
            $(currentChunk[4]).addClass('orange-border');
        }
    });
}(window.jQuery, window, document));



