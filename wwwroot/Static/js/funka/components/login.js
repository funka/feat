﻿function validateUserCredentials(data) {
    if (data.isValid) {
        window.location = data.redirectUrl;
    } else {
        displayErrorMessages(data.errorMesssage);
    }
}

function displayErrorMessages(errorMessage) {
    var form = $("body").find("#jsFunkaLoginForm");
    var outdatedValidationSummaryContainer = $(form).find(".js-validation-summary-errors");
    if (!outdatedValidationSummaryContainer.length) {
        $(form).prepend("<div class='row'><div class='js-validation-summary-errors medium-10 columns end errorOnThis'><p tabindex='-1'>" + errorMessage + "</p></div></div>");
        $(form).find(".errorOnThis > p").focus();
    } else {
        outdatedValidationSummaryContainer.hide();
        var paragraph = outdatedValidationSummaryContainer.find("p");
        $(paragraph).html(errorMessage).focus();
        outdatedValidationSummaryContainer.fadeIn("fast");
        $(paragraph).focus();
    }

    $(form).find("input[type='text'], input[type='password']").each(function () {
        $(this).parents(".form-field").parent().addClass("errorOnThis");
    });
}