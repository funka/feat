var validationTimer = null;

$(function () {
    $('form').each(function () {
        $(this).unbind("invalid-form.validate"); // remove old handler
        $(this).bind("invalid-form.validate", function (event, validator) {
            var container = $(this).find("[data-valmsg-summary=true]"),
                errorAmount = $(this).find("[data-valmsg-summary-erroramount]"),
                list = container.find("ul"),
                errorList = validator.errorList;

            if (list && list.length && errorList.length) {
                list.empty();
                container.addClass("validation-summary-errors").removeClass("validation-summary-valid");

                $.each(errorList,
                    function () {
                        $("<li />").html($("#" + this.element.id).data("label")).appendTo(list);
                    });

                errorAmount.html(errorList.length);

                clearInterval(validationTimer);
                validationTimer = null;

                validationTimer = setInterval(setFocus, 10);
            }
        });

        //Hack to replace standard message from jQuery
        $.validator.messages.email = $(this).find("input[type='email']:first").data("val-regex");
    });
});

function setFocus() {
    clearInterval(validationTimer);

    if ($("form").find("[data-valmsg-summary=true]").find("h2").length > 0) {
        $("form").find("[data-valmsg-summary=true]").find("h2").attr("tabindex", "-1").focus();
    }

    if ($("form").find("[data-valmsg-summary=true]").find("h3").length > 0) {
        $("form").find("[data-valmsg-summary=true]").find("h3").attr("tabindex", "-1").focus();
    }

    if ($("form").find("[data-valmsg-summary=true]").find("h4").length > 0) {
        $("form").find("[data-valmsg-summary=true]").find("h4").attr("tabindex", "-1").focus();
    }


}

jQuery.validator.unobtrusive.adapters.add("mandatory", function (options) {
    if (options.element.tagName.toUpperCase() === "INPUT" && options.element.type.toUpperCase() === "CHECKBOX") {
        options.rules["required"] = true;

        if (options.message) {
            options.messages["required"] = options.message;
        }
    }
});