﻿using System.Collections.Generic;
using EPiServer.Core;
using EPiServer.Framework.Localization;
using EPiServer.Validation;

namespace Furb.Features.Pages.Shared
{
    public class ContentPageDataValidator : IValidate<ContentPageData>
    {
        public IEnumerable<ValidationError> Validate(ContentPageData instance)
        {
            var validationErrors = new List<ValidationError>();

            if (instance.PrimaryImageBlock?.Image == null)
            {
                return validationErrors;
            }

            if (instance.PrimaryImageBlock.DecorativeImage)
            {
                return validationErrors;
            }

            if (string.IsNullOrEmpty(instance.PrimaryImageBlock.AltText))
            {
                validationErrors.Add(new ValidationError
                {
                    ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/IContentData/Validation/AltTextMissing"),
                    PropertyName = instance.GetPropertyName(x => x.PrimaryImageBlock.AltText),
                    Severity = ValidationErrorSeverity.Error,
                    ValidationType = ValidationErrorType.AttributeMatched
                });
            }
            else
            {
                if (instance.PrimaryImageBlock.AltText.Replace(".", "").Replace(" ", "").Trim().Length == 0)
                {
                    validationErrors.Add(new ValidationError
                    {
                        ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/IContentData/Validation/AltTextMissing"),
                        PropertyName = instance.GetPropertyName(x => x.PrimaryImageBlock.AltText),
                        Severity = ValidationErrorSeverity.Error,
                        ValidationType = ValidationErrorType.AttributeMatched
                    });
                }
                else
                {
                    if (instance.PrimaryImageBlock.AltText.Replace(".", "").Replace(" ", "").Trim().Length < 5)
                    {
                        validationErrors.Add(new ValidationError
                        {
                            ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/IContentData/Validation/AltTextToShort"),
                            PropertyName = instance.GetPropertyName(x => x.PrimaryImageBlock.AltText),
                            Severity = ValidationErrorSeverity.Warning,
                            ValidationType = ValidationErrorType.AttributeMatched
                        });
                    }
                }
            }

            return validationErrors;
        }
    }
}