﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Furb.Features.Media.Image;
using Furb.Main;
using Furb.Main.Lib.Epi.Interfaces;
using Furb.Main.Lib.MobileMenu.Interfaces;

namespace Furb.Features.Pages.Shared
{
    public abstract class SitePageData : PageData, IHidableFromAlphanumericList, IMetaData, ISocialMetaData, IMobileSitePageData
    {
        [Display(GroupName = TabNames.PageSettings, Order = 1100)]
        public virtual bool HideInAlphanumericListing { get; set; }

        [Display(GroupName = TabNames.PageSettings, Order = 1200)]
        public virtual string CustomClass { get; set; }

        [Display(GroupName = TabNames.MetaData, Order = 1100)]
        [CultureSpecific]
        public virtual string MetaTitle
        {
            get
            {
                var metaTitle = this.GetPropertyValue(p => p.MetaTitle);

                return !string.IsNullOrWhiteSpace(metaTitle) ? metaTitle : PageName;
            }

            set
            {
                this.SetPropertyValue(p => p.MetaTitle, value);
            }
        }

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(GroupName = TabNames.MetaData, Order = 1200)]
        public virtual string MetaDescription { get; set; }

        [Display(GroupName = TabNames.MetaData, Order = 1300)]
        public virtual bool DisableIndexing { get; set; }

        [CultureSpecific]
        [Display(GroupName = TabNames.MetaData, Order = 1400)]
        public virtual string SocialTitle
        {
            get
            {
                var socialTitle = this.GetPropertyValue(p => p.SocialTitle);

                return !string.IsNullOrWhiteSpace(socialTitle) ? socialTitle : MetaTitle;
            }
            set
            {
                this.SetPropertyValue(p => p.SocialTitle, value);
            }
        }

        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(GroupName = TabNames.MetaData, Order = 1500)]
        public virtual string SocialDescription
        {
            get
            {
                var socialDescription = this.GetPropertyValue(p => p.SocialDescription);

                return !string.IsNullOrWhiteSpace(socialDescription) ? socialDescription : MetaDescription;
            }

            set
            {
                this.SetPropertyValue(p => p.SocialDescription, value);
            }
        }

        [CultureSpecific]
        [AllowedTypes(AllowedTypes = new[] { typeof(ImageFile) })]
        [Display(GroupName = TabNames.MetaData, Order = 1600)]
        public virtual ContentReference SocialImageUrl { get; set; }

        [CultureSpecific]
        [Display(GroupName = TabNames.MetaData, Order = 1700)]
        public virtual PageReference SocialOverrideUrl { get; set; }

        [Display(GroupName = TabNames.PageSettings, Order = 1700)]
        public virtual bool HideInPageList { get; set; }
    }
}