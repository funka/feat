﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Furb.Features.LocalBlocks.Content;
using Furb.Features.LocalBlocks.Image;
using Furb.Features.LocalBlocks.PageTeaser;
using Furb.Main;
using Furb.Main.Lib.Epi.Interfaces;

namespace Furb.Features.Pages.Shared
{
    [AvailableContentTypes(Include = new[] { typeof(ContentPageData) })]
    public abstract class ContentPageData : SitePageData, IHasTeaser<PageTeaserBlock>, IHasMainImage<ImageBlock>, IHasMainContent<ContentBlock>
    {
        [CultureSpecific]
        [Display(GroupName = TabNames.Content, Order = 2000)]
        public virtual ContentArea TopContentArea { get; set; }

        [Display(GroupName = TabNames.Content, Order = 2100)]
        public virtual ImageBlock PrimaryImageBlock { get; set; }

        [Display(GroupName = TabNames.Content, Order = 2200)]
        public virtual ContentBlock MainContentBlock { get; set; }

        [Display(GroupName = TabNames.MoreContent, Order = 2300)]
        public virtual PageTeaserBlock TeaserBlock { get; set; }

        [Display(GroupName = TabNames.PageSettings, Order = 2400)]
        public virtual bool HideShareLinks { get; set; }
    }
}