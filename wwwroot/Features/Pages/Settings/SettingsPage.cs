﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Furb.Main;
using Furb.Main.Lib.Epi.Rendering.ContentIcon;
using Furb.Main.Lib.EpiHidePropertiesAndTabs.VisibilityAttributes;

namespace Furb.Features.Pages.Settings
{
    [ContentIcon(ContentIcon.Settings)]
    [ContentType(GUID = "f048934f-c454-41e6-b9a8-6b2893e87e4e", AvailableInEditMode = false, GroupName = ContentTypeNames.Specialized, Order = 8000)]
    [ImageUrl("~/Features/Pages/Settings/Static/SettingsPage.png")]
    public class SettingsPage : PageData
    {
        [CultureSpecific]
        [Display(GroupName = TabNames.SiteSettings, Order = 1200)]
        [ShowPropertyWhenValueEquals("ImportantNoticeText", true)]
        public virtual bool ImportantNoticeActive { get; set; }

        [CultureSpecific]
        [Display(GroupName = TabNames.SiteSettings, Order = 1300)]
        public virtual XhtmlString ImportantNoticeText { get; set; }

        [UIHint(UIHint.Textarea)]
        [Display(GroupName = TabNames.SiteSettings, Order = 2100)]
        public virtual string GoogleAnalyticsScript { get; set; }

        [Display(GroupName = TabNames.MetaData, Order = 2100)]
        public virtual string SiteTileName { get; set; }

        [Display(GroupName = TabNames.MetaData, Order = 2200)]
        public virtual string SocialSiteName { get; set; }

        [Display(GroupName = TabNames.MetaData, Order = 2300)]
        public virtual string TwitterCardType { get; set; }

        [Display(GroupName = TabNames.MetaData, Order = 2400)]
        public virtual string TwitterOwner { get; set; }

        [Display(GroupName = TabNames.MetaData, Order = 2500)]
        public virtual string FacebookPage { get; set; }
    }
}