﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Furb.Features.Pages.Shared;
using Furb.Main;
using System.ComponentModel.DataAnnotations;

namespace Furb.Features.Pages.Standard
{
    [ContentType(GUID = "89f62533-1f98-4fe4-9539-2738c3dcfc2b", GroupName = ContentTypeNames.Content, Order = 1000)]
    [AvailableContentTypes(Availability.Specific, Include = new[] { typeof(PageData) })]
    [ImageUrl("~/Features/Pages/Standard/Static/StandardPage.png")]
    public class StandardPage : ContentPageData
    {
        [CultureSpecific]
        [Display(GroupName = TabNames.Content, Order = 3300)]
        public virtual ContentArea MainContentArea { get; set; }
    }
}