using Furb.Main.ViewModels;

namespace Furb.Features.Pages.Event
{
    public class EventPageViewModel : PageViewModel<EventPage>
    {
        public EventPageViewModel(EventPage currentPage)
            : base(currentPage)
        {
        }
    }
}