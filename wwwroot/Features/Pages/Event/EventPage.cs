using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Furb.Features.LocalBlocks.EventInformation;
using Furb.Features.Pages.Shared;
using Furb.Main;
using Furb.Main.Lib.Epi.Interfaces;
using Furb.Main.Lib.Epi.Rendering.ContentIcon;

namespace Furb.Features.Pages.Event
{
    [ContentIcon(ContentIcon.Clock)]
    [ContentType(GUID = "5e839533-c8d0-46ad-b15e-cecb7cfb8fcc", GroupName = ContentTypeNames.Content, Order = 1000)]
    [ImageUrl("~/Features/Pages/Event/Static/EventPage.png")]
    public class EventPage : ContentPageData, IHasEvent<EventInformationBlock>
    {
        [ScaffoldColumn(false)]
        [Display(GroupName = TabNames.Content, Order = 2000)]
        public override ContentArea TopContentArea { get; set; }

        [Display(GroupName = TabNames.Content, Order = 2150)]
        public virtual EventInformationBlock EventInformation { get; set; }

        [CultureSpecific]
        [Display(GroupName = TabNames.Content, Order = 3100)]
        public virtual ContentArea MainContentArea { get; set; }

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            VisibleInMenu = false;
        }
    }
}