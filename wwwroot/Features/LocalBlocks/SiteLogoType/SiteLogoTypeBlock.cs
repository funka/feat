﻿using System.ComponentModel.DataAnnotations;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Furb.Features.Blocks.Shared;
using Furb.Main;

namespace Furb.Features.LocalBlocks.SiteLogoType
{
    [ContentType(GUID = "09854019-91A5-4B93-8623-17F038346001", AvailableInEditMode = false, GroupName = ContentTypeNames.Local, Order = 9000)]

    public class SiteLogoTypeBlock : SiteBlockData
    {
        [CultureSpecific]
        [UIHint(UIHint.Image)]
        [Display(Order = 2100)]
        public virtual Url ImageUrl { get; set; }

        [CultureSpecific]
        [Display(Order = 2200)]
        public virtual string ImageAltText { get; set; }

        [CultureSpecific]
        [Display(Order = 2300)]
        public virtual PageReference PageLinkReference { get; set; }
    }
}
