﻿using System.Web.Mvc;
using EPiServer.Web.Mvc;

namespace Furb.Features.LocalBlocks.Editor
{
    public class EditorBlockController : BlockController<EditorBlock>
    {
        public override ActionResult Index(EditorBlock currentBlock)
        {
            return PartialView("~/Features/LocalBlocks/Editor/Index.cshtml", currentBlock);
        }
    }
}
