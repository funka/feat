﻿using System.Web.Mvc;
using EPiServer.Web.Mvc;

namespace Furb.Features.LocalBlocks.EditorWithHeader
{
    public class EditorWithHeaderBlockController : BlockController<EditorWithHeaderBlock>
    {
        public override ActionResult Index(EditorWithHeaderBlock currentBlock)
        {
            return PartialView("~/Features/LocalBlocks/EditorWithHeader/Index.cshtml", currentBlock);
        }
    }
}
