﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using Furb.Features.Blocks.Shared;
using Furb.Main;

namespace Furb.Features.LocalBlocks.EditorWithHeader
{
    [ContentType(GUID = "95423e13-6701-4baa-9d9f-0d082edd5aeb", AvailableInEditMode = false, GroupName = ContentTypeNames.Local, Order = 9000)]
    public class EditorWithHeaderBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(Order = 2000)]
        public virtual string Header { get; set; }

        [CultureSpecific]
        [Display(Order = 2100)]
        public virtual XhtmlString Text { get; set; } 
    }
}