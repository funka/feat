﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Furb.Features.Blocks.Shared;
using Furb.Main;
using Furb.Main.Lib.Epi.Interfaces;

namespace Furb.Features.LocalBlocks.Content
{
    [ContentType(GUID = "0580ad08-9f14-4554-8f29-89b37c511413", AvailableInEditMode = false, GroupName = ContentTypeNames.Local, Order = 9000)]
    public class ContentBlock : SiteBlockData, IContentBlock
    {
        [CultureSpecific]
        [UIHint(UIHint.Textarea)]
        [Display(Order = 2100)]
        public virtual string Intro { get; set; }

        [CultureSpecific]
        [Display(Order = 2200)]
        public virtual XhtmlString MainBody { get; set; }
    }
}