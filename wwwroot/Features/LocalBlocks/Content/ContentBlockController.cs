﻿using System.Web.Mvc;
using EPiServer.Web.Mvc;

namespace Furb.Features.LocalBlocks.Content
{
    public class ContentBlockController : BlockController<ContentBlock>
    {
        public override ActionResult Index(ContentBlock currentBlock)
        {
            return PartialView("~/Features/LocalBlocks/Content/Index.cshtml", currentBlock);
        }
    }
}