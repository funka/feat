﻿using EPiServer.PlugIn;
using EPiServer.Shell.ObjectEditing;
using Furb.Main.Lib.Epi;

namespace Furb.Features.LocalBlocks.Footer
{
    public class SocialLinkItem
    {
        public string Text { get; set; }

        public string Link { get; set; }

        [SelectOne(SelectionFactoryType = typeof(SocialLinkCssFactory))]
        public string CssClass { get; set; }
    }

    [PropertyDefinitionTypePlugIn]
    public class SocialLinkItemProperty : PropertyListBase<SocialLinkItem>
    {
    }
}