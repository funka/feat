﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EPiServer.Shell.ObjectEditing;

namespace Furb.Features.LocalBlocks.Footer
{
    public class SocialLinkCssFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            var colorThemes = new List<string>
            {
                "facebook",
                "twitter",
                "youtube",
                "linkedin",
                "instagram"
            };

            return colorThemes.Select(x => new SelectItem {Text = x, Value = x.ToString(CultureInfo.InvariantCulture)});
        }
    }
}