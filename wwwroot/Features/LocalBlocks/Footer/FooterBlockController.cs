﻿using System.Web.Mvc;
using EPiServer.Web.Mvc;

namespace Furb.Features.LocalBlocks.Footer
{
    public class FooterBlockController : BlockController<FooterBlock>
    {
        public override ActionResult Index(FooterBlock currentBlock)
        {
            return PartialView("~/Features/LocalBlocks/Footer/Index.cshtml", currentBlock);
        }
    }
}