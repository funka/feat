﻿using System.Collections.Generic;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using Furb.Features.Pages.Shared;
using Furb.Main.Lib.EpiHidePropertiesAndTabs.LayoutVisibilityResolver;

namespace Furb.Features.LocalBlocks.Image
{
    [ServiceConfiguration(typeof(ILayoutVisibilityResolver))]
    public class ImageBlockLayoutVisibilityResolver : ILayoutVisibilityResolver
    {
        public IEnumerable<string> GetHiddenTabs(IContent content)
        {
            return new List<string>();
        }

        public IEnumerable<string> GetHiddenProperties(IContent content)
        {
            var hiddenProperties = new List<string>();

            var contentPageData = content as ContentPageData;

            if (contentPageData?.PrimaryImageBlock == null)
            {
                return hiddenProperties;
            }

            if (contentPageData.PrimaryImageBlock.Image == null)
            {
                hiddenProperties.Add("PrimaryImageBlock.AltText");
                hiddenProperties.Add("PrimaryImageBlock.ImageDescription");
                hiddenProperties.Add("PrimaryImageBlock.DecorativeImage");
            }
            else
            {
                if (contentPageData.PrimaryImageBlock.DecorativeImage)
                {
                    hiddenProperties.Add("PrimaryImageBlock.AltText");
                    hiddenProperties.Add("PrimaryImageBlock.ImageDescription");
                }
            }

            return hiddenProperties;
        }
    }
}