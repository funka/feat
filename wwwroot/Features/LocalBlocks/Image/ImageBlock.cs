﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using Furb.Features.Blocks.Shared;
using Furb.Features.Media.Image;
using Furb.Main;
using Furb.Main.Lib.Epi.Interfaces;
using Furb.Main.Lib.EpiHidePropertiesAndTabs;

namespace Furb.Features.LocalBlocks.Image
{
    [ContentType(GUID = "0334ca82-cf7a-4b24-98cb-4c59e8e4c2e9", AvailableInEditMode = false, GroupName = ContentTypeNames.Local, Order = 9000)]
    [ImageUrl("~/Features/LocalBlocks/Image/Static/ImageBlock.png")]
    public class ImageBlock : SiteBlockData, IImageBlock
    {
        [UIHint(UIHint.Image)]
        [AllowedTypes(AllowedTypes = new []{typeof(ImageFile)})]
        [Display(Order = 2100)]
        [RequiresLayoutRefresh]
        public virtual ContentReference Image { get; set; }

        [Display(Order = 2150)]
        [RequiresLayoutRefresh]
        public virtual bool DecorativeImage { get; set; }

        [CultureSpecific]
        [Display(Order = 2200)]
        public virtual string AltText { get; set; }

        [Display(Order = 2300)]
        [CultureSpecific]
        public virtual string ImageDescription { get; set; }
    }
}