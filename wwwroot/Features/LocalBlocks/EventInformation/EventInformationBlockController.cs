﻿using System.Web.Mvc;
using EPiServer.Web.Mvc;

namespace Furb.Features.LocalBlocks.EventInformation
{
    public class EventInformationBlockController : BlockController<EventInformationBlock>
    {
        public override ActionResult Index(EventInformationBlock currentBlock)
        {
            return PartialView("~/Features/LocalBlocks/EventInformation/Index.cshtml", currentBlock);
        }
    }
}