﻿using EPiServer.DataAnnotations;
using Furb.Features.Blocks.Shared;
using Furb.Main;
using System.ComponentModel.DataAnnotations;

namespace Furb.Features.LocalBlocks.SocialMediaShareLinks
{
    [ContentType(GUID = "AF7E33F7-5599-4362-910E-6965E8C596A1", AvailableInEditMode = false, GroupName = ContentTypeNames.Local, Order = 9000)]
    public class SocialMediaShareLinksBlock : SiteBlockData
    {
        [CultureSpecific]
        [Display(Order = 2100)]
        public virtual bool ShowFacebookShareLink { get; set; }

        [CultureSpecific]
        [Display(Order = 2200)]
        public virtual bool ShowTwitterShareLink { get; set; }

        [CultureSpecific]
        [Display(Order = 2300)]
        public virtual bool ShowLinkedInShareLink { get; set; }
    }
}