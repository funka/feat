﻿using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc;
using EPiServer.Web.Routing;
using System.Text;
using System.Web.Mvc;
using Furb.Main.Lib.Epi.Interfaces;
using Furb.Main.Lib.Extensions;

namespace Furb.Features.LocalBlocks.SocialMediaShareLinks
{
    public class SocialMediaShareLinksBlockController : BlockController<SocialMediaShareLinksBlock>
    {
        private readonly IServiceLocator serviceLocator;

        private PageData CurrentPage { get; set; }

        public SocialMediaShareLinksBlockController(IServiceLocator serviceLocator)
        {
            this.serviceLocator = serviceLocator;
        }

        private SocialMediaShareLinksBlockViewModel SetupModel(SocialMediaShareLinksBlock currentBlock)
        {
            CurrentPage = serviceLocator.GetInstance<IPageRouteHelper>().Page;

            var url = new UrlBuilder(UrlResolver.Current.GetUrl(CurrentPage.ContentLink));

            var socialMetaData = CurrentPage as ISocialMetaData;

            if (socialMetaData?.SocialOverrideUrl != null)
            {
                url = new UrlBuilder(UrlResolver.Current.GetUrl(socialMetaData.SocialOverrideUrl));
            }

            Global.UrlRewriteProvider.ConvertToExternal(url, CurrentPage.PageLink, Encoding.UTF8);
            var linkUrl = StringExtensions.GetBaseUrl() + url;

            var urlResolver = ServiceLocator.Current.GetInstance<UrlResolver>();
            var pageUrl = urlResolver.GetUrl(CurrentPage.ContentLink);
            var actionUrl = $"{pageUrl}ShareFormPost/";

            var model = new SocialMediaShareLinksBlockViewModel(currentBlock)
            {
                ShareLinkUrl = linkUrl,
                ActionUrl = actionUrl,
                PageReference = CurrentPage.PageLink,
                PageTitle = CurrentPage.PageName,
                IsActiv = currentBlock.ShowFacebookShareLink || currentBlock.ShowLinkedInShareLink || currentBlock.ShowTwitterShareLink
            };

            return model;
        }

        public override ActionResult Index(SocialMediaShareLinksBlock currentBlock)
        {
            return PartialView("~/Features/LocalBlocks/SocialMediaShareLinks/Index.cshtml", SetupModel(currentBlock));
        }
    }
}