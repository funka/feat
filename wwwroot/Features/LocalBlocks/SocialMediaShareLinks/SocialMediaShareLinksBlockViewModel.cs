﻿using EPiServer.Core;

namespace Furb.Features.LocalBlocks.SocialMediaShareLinks
{
    public class SocialMediaShareLinksBlockViewModel
    {
        public SocialMediaShareLinksBlock CurrentBlock { get; }

        public bool IsActiv { get; set; }

        public string ShareLinkUrl { get; set; }

        public string PageTitle { get; set; }

        public string ActionUrl { get; set; }

        public PageReference PageReference { get; set; }

        public SocialMediaShareLinksBlockViewModel(SocialMediaShareLinksBlock socialMediaShareLinksBlock)
        {
            CurrentBlock = socialMediaShareLinksBlock;
        }
    }
}