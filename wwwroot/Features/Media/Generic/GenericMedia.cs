﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using Furb.Main;
using Furb.Main.Lib.Epi.Interfaces;

namespace Furb.Features.Media.Generic
{
    [ContentType(GUID = "EE3BD195-7CB0-4756-AB5F-E5E223CD9820", GroupName = ContentTypeNames.Content, Order = 1000)]
    public class GenericMedia : MediaData, IHasDescription
    {
        [CultureSpecific]
        public virtual string Description { get; set; }
    }
}