﻿using System.Web.Mvc;
using EPiServer.Web.Mvc;
using Furb.Main.Lib.Epi.Interfaces;
using Furb.Main.Mappers;

namespace Furb.Features.Media.Image
{
    public class ImageFileController : PartialContentController<ImageFile>
    {
        private readonly IMapToNew<ImageFile, ImageViewModel> mapper;

        public ImageFileController(ImageFileToImageViewModelMapper mapper)
        {
            this.mapper = mapper;
        }

        public override ActionResult Index(ImageFile currentContent)
        {
            return PartialView("~/Features/Media/Image/Index.cshtml", mapper.Map(currentContent));
        }
    }
}