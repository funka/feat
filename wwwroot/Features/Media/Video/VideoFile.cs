﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Framework.DataAnnotations;
using Furb.Main;
using Furb.Main.Lib.Epi.Interfaces;

namespace Furb.Features.Media.Video
{
    [ContentType(GUID = "85468104-E06F-47E5-A317-FC9B83D3CBA6", GroupName = ContentTypeNames.Content, Order = 1000)]
    [MediaDescriptor(ExtensionString = "flv,mp4,mp3")]
    public class VideoFile : VideoData, IHasDescription
    {
        [CultureSpecific] public virtual string Description { get; set; }
    }
}