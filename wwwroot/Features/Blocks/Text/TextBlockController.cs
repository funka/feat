using System.Web.Mvc;
using EPiServer.Web.Mvc;

namespace Furb.Features.Blocks.Text
{
    public class TextBlockController : BlockController<TextBlock>
    {
        public override ActionResult Index(TextBlock currentBlock)
        {
            var model = new TextBlockViewModel(currentBlock);
            
            return PartialView("~/Features/Blocks/Text/Index.cshtml", model);
        }
    }
}