﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.Framework.Localization;
using EPiServer.Validation;
using Furb.Main.Lib.EpiExtensions;

namespace Furb.Features.Blocks.EventList
{
    public class EventListBlockValidator : IValidate<EventListBlock>
    {
        public IEnumerable<ValidationError> Validate(EventListBlock instance)
        {
            var validationErrors = new List<ValidationError>();

            if (instance.FilterList == null || !instance.FilterList.Any())
            {
                validationErrors.Add(new ValidationError
                {
                    ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/EventListBlock/Validation/MissingFilterRules"),
                    PropertyName = instance.GetPropertyName(x => x.FilterList),
                    Severity = ValidationErrorSeverity.Error,
                    ValidationType = ValidationErrorType.AttributeMatched
                });
            }
            else
            {
                if (!instance.FilterList.Any(x => x.IsDefault) || instance.FilterList.Count(x => x.IsDefault) > 1)
                {
                    validationErrors.Add(new ValidationError
                    {
                        ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/EventListBlock/Validation/OneDefaultValue"),
                        PropertyName = instance.GetPropertyName(x => x.FilterList),
                        Severity = ValidationErrorSeverity.Error,
                        ValidationType = ValidationErrorType.AttributeMatched
                    });
                }
            }

            if (instance.ShowRootLink)
            {
                if (instance.RootLink == null || instance.RootLink.IsNullOrEmpty())
                {
                    validationErrors.Add(new ValidationError
                    {
                        ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/EventListBlock/Validation/SelectRootLink"),
                        PropertyName = instance.GetPropertyName(x => x.RootLink),
                        Severity = ValidationErrorSeverity.Error,
                        ValidationType = ValidationErrorType.AttributeMatched
                    });
                }
            }

            return validationErrors;
        }
    }
}