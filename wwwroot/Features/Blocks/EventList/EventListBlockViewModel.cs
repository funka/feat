using System.Collections.Generic;
using System.Web.Mvc;
using EPiServer.Core;
using Furb.Features.Pages.Event;
using Furb.Main.ViewModels;

namespace Furb.Features.Blocks.EventList
{
    public class EventListBlockViewModel : BlockViewModel<EventListBlock>
    {
        public IEnumerable<SelectListItem> TimeSpan { get; set; }

        public IEnumerable<EventPage> Pages { get; set; }

        public PageReference CurrentPage { get; set; }

        public EventListBlockViewModel(EventListBlock currentBlock)
            : base(currentBlock)
        {
        }
    }
}