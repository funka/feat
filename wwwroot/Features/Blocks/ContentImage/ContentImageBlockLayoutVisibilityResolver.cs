﻿using System.Collections.Generic;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using Furb.Main.Lib.EpiHidePropertiesAndTabs.LayoutVisibilityResolver;

namespace Furb.Features.Blocks.ContentImage
{
    [ServiceConfiguration(typeof(ILayoutVisibilityResolver))]
    public class ContentImageBlockLayoutVisibilityResolver : ILayoutVisibilityResolver
    {
        public IEnumerable<string> GetHiddenTabs(IContent content)
        {
            return new List<string>();
        }

        public IEnumerable<string> GetHiddenProperties(IContent content)
        {
            var hiddenProperties = new List<string>();

            var contentPageData = content as ContentImageBlock;

            if (contentPageData == null)
            {
                return hiddenProperties;
            }

            if (contentPageData.DecorativeImage)
            {
                hiddenProperties.Add("AltText");
                hiddenProperties.Add("ImageDescription");
            }

            return hiddenProperties;
        }
    }
}