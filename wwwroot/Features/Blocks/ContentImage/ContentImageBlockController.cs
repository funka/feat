using System.Web.Mvc;
using EPiServer.Web.Mvc;

namespace Furb.Features.Blocks.ContentImage
{
    public class ContentImageBlockController : BlockController<ContentImageBlock>
    {
        public override ActionResult Index(ContentImageBlock currentBlock)
        {
            var model = new ContentImageBlockViewModel(currentBlock);
            
            return PartialView("~/Features/Blocks/ContentImage/Index.cshtml", model);
        }
    }
}