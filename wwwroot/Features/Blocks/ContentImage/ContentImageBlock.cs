using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using Furb.Features.Blocks.Shared;
using Furb.Features.Media.Image;
using Furb.Main;
using Furb.Main.Lib.Epi.Interfaces;
using Furb.Main.Lib.Epi.Rendering.ContentIcon;
using Furb.Main.Lib.EpiExtensions;
using Furb.Main.Lib.EpiHidePropertiesAndTabs;

namespace Furb.Features.Blocks.ContentImage
{
    [ContentIcon(ContentIcon.ObjectImage)]
    [ContentType(GUID = "0e9514c9-e04e-4902-8728-de6d1ba21a91", GroupName = ContentTypeNames.Content, Order = 1000)]
    [ImageUrl("~/Features/Blocks/ContentImage/Static/ImageBlock.png")]
    public class ContentImageBlock : SiteBlockData, IHasDemoSettings
    {
        [Required]
        [UIHint(UIHint.Image)]
        [Display(GroupName = TabNames.Content, Order = 300)]
        public virtual ContentReference Image { get; set; }

        [Display(GroupName = TabNames.Content, Order = 350)]
        [RequiresLayoutRefresh]
        public virtual bool DecorativeImage { get; set; }

        [CultureSpecific]
        [Display(GroupName = TabNames.Content, Order = 400)]
        public virtual string AltText { get; set; }

        [CultureSpecific]
        [Display(GroupName = TabNames.Content, Order = 500)]
        public virtual string ImageDescription { get; set; }

        public List<string> DemoNames()
        {
            return new List<string> {"Standard"};
        }

        public void SetDemoSettings(int index)
        {
            var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();
            var image = contentLoader.GetDescendantsOfType<ImageFile>(ContentReference.GlobalBlockFolder).ToList().First();

            Image = image.ContentLink;
            AltText = image.Name;
        }
    }
}