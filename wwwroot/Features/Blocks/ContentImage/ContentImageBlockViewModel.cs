using Furb.Main.ViewModels;

namespace Furb.Features.Blocks.ContentImage
{
    public class ContentImageBlockViewModel : BlockViewModel<ContentImageBlock>
    {
        public ContentImageBlockViewModel(ContentImageBlock currentBlock)
            : base(currentBlock)
        {
        }
    }
}