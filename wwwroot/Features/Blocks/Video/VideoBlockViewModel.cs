﻿using System;
using System.Text.RegularExpressions;
using Furb.Main.ViewModels;

namespace Furb.Features.Blocks.Video
{
    public class VideoBlockViewModel : BlockViewModel<VideoBlock>
    {
        public string YoutubeVideoId { get; set; }

        private static string GetYoutubeVideoId(VideoBlock block)
        {
            var url = block.VideoUrl;

            if (url.Contains("&"))
            {
                url = url.Remove(url.IndexOf("&", StringComparison.Ordinal));
            }

            return url.Contains("v=") ? Regex.Split(url, "v=")[1] : Regex.Split(url, "/")[3];
        }

        public VideoBlockViewModel(VideoBlock currentBlock) : base(currentBlock)
        {
            YoutubeVideoId = GetYoutubeVideoId(currentBlock);
        }
    }
}