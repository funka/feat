﻿using System.Collections.Generic;
using EPiServer.Core;
using EPiServer.Framework.Localization;
using EPiServer.Validation;

namespace Furb.Features.Blocks.Video
{
    public class VideoBlockValidator : IValidate<VideoBlock>
    {
        public IEnumerable<ValidationError> Validate(VideoBlock instance)
        {
            var validationErrors = new List<ValidationError>();

            if (!instance.VideoUrl.StartsWith("http"))
            {
                validationErrors.Add(new ValidationError
                {
                    ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/VideoBlock/Validation/BadVideoUrl"),
                    PropertyName = instance.GetPropertyName(x => x.VideoUrl),
                    Severity = ValidationErrorSeverity.Error,
                    ValidationType = ValidationErrorType.AttributeMatched
                });
            }
            else
            {
                if (instance.VideoPlayerType == VideoPlayer.Youtube && !instance.VideoUrl.Contains("youtu"))
                {
                    validationErrors.Add(new ValidationError
                    {
                        ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/VideoBlock/Validation/BadYoutubeUrl"),
                        PropertyName = instance.GetPropertyName(x => x.VideoUrl),
                        Severity = ValidationErrorSeverity.Warning,
                        ValidationType = ValidationErrorType.AttributeMatched
                    });
                }

                if (instance.VideoPlayerType == VideoPlayer.Vimeo && !instance.VideoUrl.Contains("vimeo"))
                {
                    validationErrors.Add(new ValidationError
                    {
                        ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/VideoBlock/Validation/BadVimeoUrl"),
                        PropertyName = instance.GetPropertyName(x => x.VideoUrl),
                        Severity = ValidationErrorSeverity.Warning,
                        ValidationType = ValidationErrorType.AttributeMatched
                    });
                }
            }

            return validationErrors;
        }
    }
}