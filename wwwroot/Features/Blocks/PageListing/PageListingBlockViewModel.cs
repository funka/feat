using System.Collections.Generic;
using EPiServer.Core;
using Furb.Main.ViewModels;

namespace Furb.Features.Blocks.PageListing
{
    public class PageListingBlockViewModel : BlockViewModel<PageListingBlock>
    {
        public string RootLinkText { get; set; }

        public IEnumerable<PageData> PageList { get; set; }

        public PageListingBlockViewModel(PageListingBlock currentBlock)
            : base(currentBlock)
        {
        }
    }
}