﻿using Furb.Features.Blocks.Shared;
using Furb.Main.Interfaces;

namespace Furb.Main.ViewModels
{
    public class BlockViewModel<T> : IBlockViewModel<T> where T : SiteBlockData
    {
        public BlockViewModel(T currentBlock)
        {
            CurrentBlock = currentBlock;
        }

        public T CurrentBlock { get; }
    }

    public static class BlockViewModel
    {
        public static BlockViewModel<T> Create<T>(T page) where T : SiteBlockData
        {
            return new BlockViewModel<T>(page);
        }
    }
}