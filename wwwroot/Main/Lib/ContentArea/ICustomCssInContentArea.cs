namespace Furb.Main.Lib.ContentArea
{
    public interface ICustomCssInContentArea
    {
        string ContentAreaCssClass { get; }
    }
}