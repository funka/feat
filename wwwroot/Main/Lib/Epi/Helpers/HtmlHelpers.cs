﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc.Html;
using EPiServer.Web.Routing;
using Furb.Main.Lib.Epi.Interfaces;
using Furb.Main.Lib.EpiExtensions;
using Furb.Main.Lib.Extensions;

namespace Furb.Main.Lib.Epi.Helpers
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString Disable(this MvcHtmlString helper, bool disabled)
        {
            if (helper == null)
            {
                throw new ArgumentNullException();
            }

            if (!disabled)
            {
                return helper;
            }

            var html = helper.ToString();
            var startIndex = html.IndexOf('>');

            html = html.Insert(startIndex, " disabled=\"disabled\"");
            return MvcHtmlString.Create(html);
        }

        public static MvcHtmlString Required(this MvcHtmlString helper, ModelMetadata modelMetadata)
        {
            if (helper == null)
            {
                throw new ArgumentNullException();
            }

            if (!modelMetadata.IsRequired)
            {
                return helper;
            }

            var html = helper.ToString();
            var startIndex = html.IndexOf('>');

            html = html.Insert(startIndex, " required=\"required\"");

            return MvcHtmlString.Create(html);
        }

        public static MvcHtmlString Type(this MvcHtmlString helper, ModelMetadata modelMetadata, object viewDataType)
        {
            if (helper == null)
            {
                throw new ArgumentNullException();
            }

            var type = string.IsNullOrEmpty(modelMetadata.DataTypeName) ? "text" : modelMetadata.DataTypeName.ToLower();

            if (viewDataType != null)
            {
                type = viewDataType.ToString();
            }
            if (type.ToLower().Contains("email"))
            {
                type = "email";
            }

            switch (modelMetadata.DataTypeName)
            {
                case "PhoneNumber":
                    type = "tel";
                    break;
            }

            var html = helper.ToString();
            var startIndex = html.IndexOf('>');

            html = html.Contains("type=") ? html.Replace("type=\"text\"", "type =\"" + type + "\"") : html.Insert(startIndex, " type=\"" + type + "\"");

            return MvcHtmlString.Create(html);
        }

        public static MvcHtmlString RemoveValue(this MvcHtmlString helper)
        {
            if (helper == null)
            {
                throw new ArgumentNullException();
            }

            var html = helper.ToString();

            if (html.Contains("value"))
            {
                html = html.Replace("value", string.Empty);
            }

            return MvcHtmlString.Create(html);
        }

        public static MvcHtmlString DescribedBy(this MvcHtmlString helper, string id)
        {
            if (helper == null)
            {
                throw new ArgumentNullException();
            }

            if (string.IsNullOrEmpty(id))
            {
                return helper;
            }

            var html = helper.ToString();
            var startIndex = html.LastIndexOf("/>", StringComparison.Ordinal) - 1;

            if (startIndex <= 0)
            {
                startIndex = html.IndexOf('>');
            }

            html = html.Insert(startIndex, " aria-describedby=\"" + id + "\"");

            return MvcHtmlString.Create(html);
        }

        public static MvcHtmlString TabIndex(this MvcHtmlString helper, ModelMetadata modelMetadata)
        {
            if (helper == null)
            {
                throw new ArgumentNullException();
            }

            if (modelMetadata.AdditionalValues != null && modelMetadata.AdditionalValues.Any() && modelMetadata.AdditionalValues.ContainsKey("TabIndex"))
            {
                var tabIndex = modelMetadata.AdditionalValues.FirstOrDefault(item => item.Key == "TabIndex").Value.ToString();

                if (string.IsNullOrEmpty(tabIndex))
                {
                    return helper;
                }

                if (!int.TryParse(tabIndex, out _))
                {
                    return helper;
                }

                var html = helper.ToString();
                var startIndex = html.IndexOf('>');

                html = html.Insert(startIndex, " tabindex=\"" + tabIndex + "\"");
                return MvcHtmlString.Create(html);
            }

            return helper;
        }

        public static MvcHtmlString AutoComplete(this MvcHtmlString helper, ModelMetadata modelMetadata, string autocomplete = "")
        {
            if (helper == null)
            {
                throw new ArgumentNullException();
            }

            if (string.IsNullOrEmpty(autocomplete) && modelMetadata.AdditionalValues != null && modelMetadata.AdditionalValues.Any() && modelMetadata.AdditionalValues.ContainsKey("AutoComplete"))
            {
                var value = modelMetadata.AdditionalValues.FirstOrDefault(item => item.Key == "AutoComplete").Value.ToString();

                if (!string.IsNullOrEmpty(value))
                {
                    autocomplete = value;
                }
            }

            if (string.IsNullOrEmpty(autocomplete))
            {
                return helper;
            }

            var html = helper.ToString();
            var startIndex = html.LastIndexOf("/>", StringComparison.Ordinal) - 1;

            if (startIndex <= 0)
            {
                startIndex = html.IndexOf('>');
            }

            html = html.Insert(startIndex, $" autocomplete=\"{autocomplete}\"");

            return MvcHtmlString.Create(html);
        }

        public static ConditionalLink BeginConditionalLink(this HtmlHelper helper, bool shouldWriteLink, IHtmlString url, string title = null, string cssClass = null)
        {
            if (shouldWriteLink)
            {
                var linkTag = new TagBuilder("a");
                linkTag.Attributes.Add("href", url.ToHtmlString());

                if (!string.IsNullOrWhiteSpace(title))
                {
                    linkTag.Attributes.Add("title", helper.Encode(title));
                }

                if (!string.IsNullOrWhiteSpace(cssClass))
                {
                    linkTag.Attributes.Add("class", cssClass);
                }

                helper.ViewContext.Writer.Write(linkTag.ToString(TagRenderMode.StartTag));
            }

            return new ConditionalLink(helper.ViewContext, shouldWriteLink);
        }

        public static ConditionalLink BeginConditionalLink(this HtmlHelper helper, bool shouldWriteLink, Func<IHtmlString> urlGetter, string title = null, string cssClass = null)
        {
            IHtmlString url = MvcHtmlString.Empty;

            if (shouldWriteLink)
            {
                url = urlGetter();
            }

            return helper.BeginConditionalLink(shouldWriteLink, url, title, cssClass);
        }

        public static IHtmlString BreadCrumbList(this HtmlHelper helper, Func<BreadCrumbItem, HelperResult> itemTemplate = null, ContentReference pageReference = null)
        {
            itemTemplate = itemTemplate ?? GetBreadCrumbTemplate(helper);
            pageReference = pageReference ?? helper.ViewContext.RequestContext.GetContentLink();

            var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();

            var breadCrumbItem = CreateBreadCrumbItem(pageReference.GetPage(), pageReference, contentLoader);

            var buffer = new StringBuilder();
            var writer = new StringWriter(buffer);

            itemTemplate(breadCrumbItem).WriteTo(writer);

            return new MvcHtmlString(buffer.ToString());
        }

        public static string GetCssClass(MenuItem menuItem)
        {
            if (menuItem.SelectedAndCurrentPage)
            {
                return "selected";
            }

            return menuItem.Selected ? "active" : string.Empty;
        }

        public static IHtmlString LinkSelectorString(this HtmlHelper helper, Func<LinkSelector, HelperResult> itemTemplate = null, string url = null)
        {
            itemTemplate = itemTemplate ?? GetLinkSelector(helper);
            url = url ?? helper.ViewContext.RequestContext.ToString();
            var linkSelectorItem = CreateLinkSelector(url);

            var buffer = new StringBuilder();
            var writer = new StringWriter(buffer);

            itemTemplate(linkSelectorItem).WriteTo(writer);

            return new MvcHtmlString(buffer.ToString());
        }

        public static IHtmlString MenuList(this HtmlHelper helper, ContentReference rootLink, Func<MenuItem, HelperResult> itemTemplate = null, bool includeRoot = false, bool requireVisibleInMenu = true, bool requirePageTemplate = true, Type type = null)
        {
            itemTemplate = itemTemplate ?? GetDefaultItemTemplate(helper);
            var currentContentLink = helper.ViewContext.RequestContext.GetContentLink();
            var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();

            Func<IEnumerable<PageData>, IEnumerable<PageData>> filter = pages => pages.FilterForDisplay(false, true);

            var pagePath = contentLoader.GetAncestors(currentContentLink).Reverse().Select(x => x.ContentLink).SkipWhile(x => !x.CompareToIgnoreWorkID(rootLink)).ToList();

            var menuItems = contentLoader.GetChildren<PageData>(rootLink).Where(x => x.IsNot<IHiddenFromMenu>()).FilterForDisplay(false, true).Select(x => CreateMenuItem(x, currentContentLink, pagePath, contentLoader, filter)).ToList();

            if (includeRoot)
            {
                menuItems.Insert(0, CreateMenuItem(contentLoader.Get<PageData>(rootLink), currentContentLink, pagePath, contentLoader, filter));
            }

            if (type != null)
            {
                menuItems = menuItems.Where(item => item.Page.GetType().IsSubclassOf(type)).ToList();
            }

            var buffer = new StringBuilder();
            var writer = new StringWriter(buffer);

            foreach (var menuItem in menuItems)
            {
                itemTemplate(menuItem).WriteTo(writer);
            }

            return new MvcHtmlString(buffer.ToString());
        }

        public static void RenderBalancedContentArea(this HtmlHelper helper, EPiServer.Core.ContentArea contentArea)
        {
            helper.RenderContentArea(contentArea);
        }

        public static void RenderContentReference(this HtmlHelper html, ContentReference contentReference)
        {
            IContentData contentData = ServiceLocator.Current.GetInstance<IContentRepository>().Get<IContent>(contentReference);
            html.RenderContentData(contentData, false, html.ViewContext.ViewData["tag"] as string);
        }

        public static MvcHtmlString ToAttributes(this IDictionary<string, object> attributeDictionary)
        {
            return MvcHtmlString.Create(string.Join(" ", attributeDictionary.Select(d => $"{d.Key}=\"{HttpUtility.HtmlAttributeEncode(d.Value.ToString())}\"")));
        }

        public static MvcHtmlString WrapLastWord(this HtmlHelper helper, string inputText, string className, string elementName)
        {
            if (inputText == null)
            {
                return new MvcHtmlString(string.Empty);
            }

            var splittedHeader = inputText.Split(' ');
            splittedHeader[splittedHeader.Length - 1] = "<" + elementName + " class='" + className + "'>" + splittedHeader.Last() + "</" + elementName + ">";

            return new MvcHtmlString(string.Join(" ", splittedHeader));
        }

        public static MvcHtmlString WrapWords(this HtmlHelper helper, string inputText, string className, string elementName, int amountOfWords)
        {
            if (string.IsNullOrWhiteSpace(inputText))
            {
                return new MvcHtmlString(string.Empty);
            }
            else if (amountOfWords <= 0)
            {
                return new MvcHtmlString(inputText);
            }
            var splittedHeader = inputText.Split(' ');

            var wrappedHeader = amountOfWords <= splittedHeader.Length ? splittedHeader.Take(amountOfWords).ToArray() : splittedHeader.Take(splittedHeader.Length).ToArray();

            for (var i = 0; i < wrappedHeader.Length; i++)
            {
                splittedHeader[i] = "<" + elementName + " class=" + className + "'>" + wrappedHeader[i] + "</" +
                                    elementName + ">";
            }

            return new MvcHtmlString(string.Join(" ", splittedHeader));
        }

        private static BreadCrumbItem CreateBreadCrumbItem(PageData page, ContentReference currentContentLink, IContentLoader contentLoader)
        {
            var breadCrumbItem = new BreadCrumbItem(page)
            {
                Ancestors = contentLoader.GetAncestors(currentContentLink).Where(x => x.ContentTypeID > 1).ToList()
            };

            return breadCrumbItem;
        }

        private static LinkSelector CreateLinkSelector(string url)
        {
            var arr = url.Split(new[] { "/" }, StringSplitOptions.None);
            var linkSelector = new LinkSelector(url, arr);

            return linkSelector;
        }

        private static MenuItem CreateMenuItem(PageData page, ContentReference currentContentLink, List<ContentReference> pagePath, IContentLoader contentLoader, Func<IEnumerable<PageData>, IEnumerable<PageData>> filter)
        {
            var menuItem = new MenuItem(page)
            {
                Selected = page.ContentLink.CompareToIgnoreWorkID(currentContentLink) || pagePath.Contains(page.ContentLink),
                SelectedAndCurrentPage = page.ContentLink.CompareToIgnoreWorkID(currentContentLink) || (pagePath.Contains(page.ContentLink) && page.PageLink.ID == currentContentLink.ID),
                HasChildren = new Lazy<bool>(() => filter(contentLoader.GetChildren<PageData>(page.ContentLink)).Any())
            };

            return menuItem;
        }

        private static Func<BreadCrumbItem, HelperResult> GetBreadCrumbTemplate(HtmlHelper helper)
        {
            return x => new HelperResult(writer => writer.Write(helper.PageLink(x.Page)));
        }

        private static Func<MenuItem, HelperResult> GetDefaultItemTemplate(HtmlHelper helper)
        {
            return x => new HelperResult(writer => writer.Write(helper.PageLink(x.Page)));
        }

        private static Func<LinkSelector, HelperResult> GetLinkSelector(HtmlHelper helper)
        {
            return x => new HelperResult(writer => writer.Write(helper.UrlLink(x.Url)));
        }

        public class BreadCrumbItem
        {
            public BreadCrumbItem(PageData page)
            {
                Page = page;
            }

            public List<IContent> Ancestors { get; set; }

            public PageData Page { get; set; }
        }

        public class ConditionalLink : IDisposable
        {
            private readonly bool linked;
            private readonly ViewContext viewContext;
            private bool disposed;

            public ConditionalLink(ViewContext viewContext, bool isLinked)
            {
                this.viewContext = viewContext;
                linked = isLinked;
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            protected virtual void Dispose(bool disposing)
            {
                if (disposed)
                {
                    return;
                }

                disposed = true;

                if (linked)
                {
                    viewContext.Writer.Write("</a>");
                }
            }
        }

        public class LinkSelector
        {
            public LinkSelector(string url, string[] listOfStrings)
            {
                Url = url;
                UrlSegments = listOfStrings;
            }

            public string Url { get; set; }

            public string[] UrlSegments { get; set; }
        }

        public class MenuItem
        {
            public MenuItem(PageData page)
            {
                Page = page;
            }

            public Lazy<bool> HasChildren { get; set; }

            public PageData Page { get; set; }

            public bool Selected { get; set; }

            public bool SelectedAndCurrentPage { get; set; }
        }
    }
}