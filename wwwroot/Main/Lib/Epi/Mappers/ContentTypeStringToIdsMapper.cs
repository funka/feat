﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Furb.Main.Lib.Epi.Mappers
{
    public static class ContentTypeStringToIdsMapper
    {
        public static List<int> Map(string commaSeparatedList)
        {
            if (commaSeparatedList == null)
            {
                throw new ArgumentNullException(nameof(commaSeparatedList));
            }

            return commaSeparatedList.Split(',').Select(int.Parse).ToList();
        }
    }
}