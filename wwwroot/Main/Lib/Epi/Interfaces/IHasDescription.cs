﻿namespace Furb.Main.Lib.Epi.Interfaces
{
    public interface IHasDescription
    {
        string Description { get; set; }
    }
}