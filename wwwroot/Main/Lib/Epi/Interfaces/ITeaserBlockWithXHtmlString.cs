﻿using EPiServer.Core;

namespace Furb.Main.Lib.Epi.Interfaces
{
    public interface ITeaserBlockWithXHtmlString : ITeaserBlock
    {
        XhtmlString TeaserHtml { get; set; }
    }
}