﻿namespace Furb.Main.Lib.Epi.Interfaces
{
    public interface IMapToNew<in TSource, out TTarget>
    {
        TTarget Map(TSource source);
    }
}