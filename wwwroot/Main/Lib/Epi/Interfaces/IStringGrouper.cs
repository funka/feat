﻿using System.Collections.Generic;
using System.Collections.Specialized;
using EPiServer.Core;

namespace Furb.Main.Lib.Epi.Interfaces
{
    public interface IStringGrouper
    {
        OrderedDictionary Group(string letters, IEnumerable<PageData> pages);
    }
}