﻿using EPiServer.Core;

namespace Furb.Main.Lib.Epi.Interfaces
{
    public interface IHasTeaser<T> where T : BlockData, ITeaserBlock
    {
        T TeaserBlock { get; set; }
    }
}