﻿namespace Furb.Main.Lib.Epi.Interfaces
{
    public interface IHasAltText
    {
        string AltText { get; set; }
    }
}