﻿using EPiServer.Core;

namespace Furb.Main.Lib.Epi.Interfaces
{
    public interface IHasMainImage<T> where T : BlockData, IImageBlock
    {
        T PrimaryImageBlock { get; set; }
    }
}