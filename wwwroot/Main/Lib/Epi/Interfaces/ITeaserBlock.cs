﻿using System.ComponentModel.DataAnnotations;

namespace Furb.Main.Lib.Epi.Interfaces
{
    public interface ITeaserBlock
    {
        string LinkText { get; set; }

        [UIHint(SiteUiHints.LineBreakString)]
        string TeaserText { get; set; }
    }
}