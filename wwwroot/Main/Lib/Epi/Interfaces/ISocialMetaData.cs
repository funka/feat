﻿using EPiServer.Core;

namespace Furb.Main.Lib.Epi.Interfaces
{
    public interface ISocialMetaData
    {
        string SocialTitle { get; set; }

        string SocialDescription { get; set; }

        ContentReference SocialImageUrl { get; set; }

        PageReference SocialOverrideUrl { get; set; }
    }
}