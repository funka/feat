﻿using EPiServer.Core;

namespace Furb.Main.Lib.Epi.Interfaces
{
    public interface IImageBlock
    {
        ContentReference Image { get; set; }

        string AltText { get; set; }
    }
}