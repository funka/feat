﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.Filters;
using Furb.Main.Lib.EpiExtensions;

namespace Furb.Main.Lib.Epi.Blocks.PageListBlock
{
    public class PageListBlockLogic
    {
        private readonly IContentLoader contentLoader;

        public PageListBlockLogic(IContentLoader contentLoader)
        {
            this.contentLoader = contentLoader;
        }

        public IEnumerable<PageData> GetFilteredPageListForContentReference(ContentReference reference, bool recursive, ICollection<int> typesToInclude = null, CategoryList categoryList = null)
        {
            if (reference == null)
            {
                return new List<PageData>();
            }

            var pageList = recursive ? contentLoader.GetDescendantsOfType<PageData>(reference).ToList() : contentLoader.GetChildren<PageData>(reference);

            return GetFilteredPageList(typesToInclude, categoryList, pageList);
        }

        public IEnumerable<PageData> GetFilteredPageListForContentArea(EPiServer.Core.ContentArea contentArea, ICollection<int> typesToInclude = null, CategoryList categoryList = null)
        {
            var pageList = contentArea.GetFilteredItemsOfType<PageData>();

            return GetFilteredPageList(typesToInclude, categoryList, pageList);
        }

        public IEnumerable<PageData> Sort(IEnumerable<PageData> pages, FilterSortOrder sortOrder)
        {
            var asCollection = new PageDataCollection(pages);
            var sortFilter = new FilterSort(sortOrder);

            sortFilter.Sort(asCollection);

            return asCollection;
        }

        private static IEnumerable<PageData> GetFilteredPageList(ICollection<int> typesToInclude, CategoryList categoryList, IEnumerable<PageData> pageList)
        {
            if (categoryList != null && categoryList.Any())
            {
                pageList = pageList.Where(x => x.Category.Intersect(categoryList).Any());
            }

            var filteredList = FilterForVisitor.Filter(pageList).ToList();

            if (typesToInclude != null && typesToInclude.Any())
            {
                filteredList = filteredList.Where(page => typesToInclude.Contains(page.ContentTypeID)).ToList();
            }

            return filteredList.Cast<PageData>();
        }
    }
}