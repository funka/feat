﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EPiServer.Framework.Localization;
using Furb.Main.Lib.Epi.Interfaces;

namespace Furb.Main.Lib.Epi.Pages.EventListPage
{
    public class EventListPageLogic
    {
        public static bool IsVisible(IHasEventDate eventPage, string selectedValue)
        {
            var amount = Convert.ToInt32(selectedValue.Split(' ')[0]);
            var what = selectedValue.Split(' ')[1].ToLower();

            switch (what)
            {
                case "day":
                    return (eventPage.StartTime.Date < DateTime.Now.Date && eventPage.EndTime.Date.AddDays(1) > DateTime.Now.Date) ||
                           (eventPage.StartTime.Date >= DateTime.Now.Date && eventPage.StartTime < DateTime.Now.AddDays(amount));

                case "month":
                    return (eventPage.StartTime.Date < DateTime.Now.Date && eventPage.EndTime.Date.AddDays(1) > DateTime.Now.Date) ||
                           (eventPage.StartTime.Date >= DateTime.Now.Date && eventPage.StartTime < DateTime.Now.AddMonths(amount));

                case "year":
                    return (eventPage.StartTime.Date < DateTime.Now.Date && eventPage.EndTime.Date.AddDays(1) > DateTime.Now.Date) ||
                           (eventPage.StartTime.Date >= DateTime.Now.Date && eventPage.StartTime < DateTime.Now.AddYears(amount));

                default:
                    throw new ArgumentOutOfRangeException(selectedValue, "SelectValue must contain year or month!");
            }
        }

        public IEnumerable<SelectListItem> TimeSpanList(IEnumerable<TimeSpanItem> timeFilter, string selectedValue)
        {
            return timeFilter.Select(timeSpanItem => new SelectListItem
            {
                Text = GetText(timeSpanItem),
                Value = timeSpanItem.ValueString(),
                Selected = timeSpanItem.ValueString() == selectedValue
            }).ToList();
        }

        private static string GetText(TimeSpanItem timeSpanItem)
        {
            if (!string.IsNullOrEmpty(timeSpanItem.Text))
            {
                return timeSpanItem.Text;
            }

            var countText = timeSpanItem.Count.ToString();

            if (LocalizationService.Current.TryGetString("/event/count/nr" + timeSpanItem.Count, out var countLocalizedString))
            {
                countText = countLocalizedString;
            }

            if (LocalizationService.Current.TryGetString("/event/count/nr" + timeSpanItem.Count + timeSpanItem.Type, out var countLocalizedStringSpecial))
            {
                countText = countLocalizedStringSpecial;
            }

            if (timeSpanItem.Count == 1)
            {
                return countText + " " + LocalizationService.Current.GetString("/event/type1/" + timeSpanItem.Type);
            }

            return countText + " " + LocalizationService.Current.GetString("/event/type/" + timeSpanItem.Type);
        }
    }
}