﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using Furb.Main.Lib.Epi.SelectionFactories;

namespace Furb.Main.Lib.Epi.Pages.EventListPage
{
    public class TimeSpanItem
    {
        [Required]
        [Searchable(false)]
        [SelectOne(SelectionFactoryType = typeof(EnumSelectionFactory<TimeSpanType>))]
        public virtual TimeSpanType Type { get; set; }

        [Required]
        [Searchable(false)]
        public int Count { get; set; }

        [Searchable(false)]
        public string Text { get; set; }

        [Searchable(false)]
        public bool IsDefault { get; set; }

        public string ValueString()
        {
            return Count + " " + Enum.GetName(typeof(TimeSpanType), Type);
        }
    }
}