﻿namespace Furb.Main.Lib.Epi.Pages.EventListPage
{
    public enum TimeSpanType
    {
        Day = 0,
        Month = 1,
        Year = 2
    }
}