﻿using System.Collections.Generic;
using System.Web.Mvc;
using Furb.Main.Lib.Epi.Attributes;

namespace Furb.Main.Lib.Epi.Adapters
{
    public class LocalizedStringLengthAdapter : DataAnnotationsModelValidator<LocalizedStringLengthAttribute>
    {
        public LocalizedStringLengthAdapter(ModelMetadata metadata, ControllerContext context, LocalizedStringLengthAttribute attribute) : base(metadata, context, attribute)
        {
        }

        public override IEnumerable<ModelClientValidationRule> GetClientValidationRules()
        {
            return new[] {new ModelClientValidationStringLengthRule(ErrorMessage, Attribute.MinimumLength, Attribute.MaximumLength)};
        }
    }
}