﻿using System.Collections.Generic;
using System.Web.Mvc;
using Furb.Main.Lib.Epi.Attributes;

namespace Furb.Main.Lib.Epi.Adapters
{
    public class LocalizedCompareAdapter : DataAnnotationsModelValidator<LocalizedCompareAttribute>
    {
        public LocalizedCompareAdapter(ModelMetadata metadata, ControllerContext context, LocalizedCompareAttribute attribute) : base(metadata, context, attribute)
        {
        }

        public override IEnumerable<ModelClientValidationRule> GetClientValidationRules()
        {
            return new[] {new ModelClientValidationEqualToRule(ErrorMessage, Attribute.OtherProperty)};
        }
    }
}