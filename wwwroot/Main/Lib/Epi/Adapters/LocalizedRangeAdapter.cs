﻿using System.Collections.Generic;
using System.Web.Mvc;
using Furb.Main.Lib.Epi.Attributes;

namespace Furb.Main.Lib.Epi.Adapters
{
    public class LocalizedRangeAdapter : DataAnnotationsModelValidator<LocalizedRangeAttribute>
    {
        public LocalizedRangeAdapter(ModelMetadata metadata, ControllerContext context, LocalizedRangeAttribute attribute) : base(metadata, context, attribute)
        {
        }

        public override IEnumerable<ModelClientValidationRule> GetClientValidationRules()
        {
            return new[] {new ModelClientValidationRangeRule(ErrorMessage, Attribute.Minimum, Attribute.Maximum)};
        }
    }
}