﻿using System.Collections.Generic;
using System.Web.Mvc;
using Furb.Main.Lib.Epi.Attributes;

namespace Furb.Main.Lib.Epi.Adapters
{
    public class LocalizedRegularExpressionAdapter : DataAnnotationsModelValidator<LocalizedRegularExpressionAttribute>
    {
        public LocalizedRegularExpressionAdapter(ModelMetadata metadata, ControllerContext context, LocalizedRegularExpressionAttribute attribute) : base(metadata, context, attribute)
        {
        }

        public override IEnumerable<ModelClientValidationRule> GetClientValidationRules()
        {
            return new[] {new ModelClientValidationRegexRule(ErrorMessage, Attribute.Pattern)};
        }
    }
}