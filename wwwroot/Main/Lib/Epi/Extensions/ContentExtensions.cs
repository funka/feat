﻿using System;
using System.Web;
using EPiServer.Core;
using Furb.Main.Lib.Epi.Interfaces;
using Furb.Main.Lib.EpiExtensions;
using Furb.Main.Lib.Extensions;

namespace Furb.Main.Lib.Epi.Extensions
{
    public static class ContentExtensions
    {
        public static string GetTeaserText(this IContent content)
        {
            if (content == null)
            {
                return string.Empty;
            }

            var teaserBlockWithXHtmlString = content.GetProperty<ITeaserBlockWithXHtmlString>();
            if (teaserBlockWithXHtmlString?.TeaserHtml != null && !string.IsNullOrEmpty(teaserBlockWithXHtmlString.TeaserHtml.ToHtmlString().StripHtml().Trim()))
            {
                return teaserBlockWithXHtmlString.TeaserHtml.ToHtmlString();
            }

            var teaserBlockWithLineBreak = content.GetProperty<ITeaserBlockWithLineBreak>();
            if (!string.IsNullOrEmpty(teaserBlockWithLineBreak?.TeaserText))
            {
                return teaserBlockWithLineBreak.TeaserText.Replace(Environment.NewLine, "<br />").Replace("\n", "<br />");
            }

            var teaserBlock = content.GetProperty<ITeaserBlock>();
            if (!string.IsNullOrEmpty(teaserBlock?.TeaserText))
            {
                return teaserBlock.TeaserText;
            }

            var contentBlock = content.GetProperty<IContentBlock>();

            if (contentBlock != null)
            {
                if (!string.IsNullOrEmpty(contentBlock.Intro))
                {
                    return contentBlock.Intro.ToIntroString(255);
                }

                if (contentBlock.MainBody != null)
                {
                    return HttpUtility.HtmlDecode(contentBlock.MainBody.ToHtmlString()).StripHtml().ToIntroString(255);
                }
            }

            var hasDescription = content as IHasDescription;
            if (!string.IsNullOrEmpty(hasDescription?.Description))
            {
                return hasDescription.Description;
            }

            if (content.Property["TeaserText"]?.Value is XhtmlString teaserTextHtml)
            {
                return HttpUtility.HtmlDecode(teaserTextHtml.ToHtmlString()).StripHtml().ToIntroString(255);
            }

            if (content.Property["TeaserText"]?.Value is string teaserText)
            {
                return HttpUtility.HtmlDecode(teaserText);
            }

            var intro = content.Property["Intro"]?.Value as string;
            if (!string.IsNullOrEmpty(intro))
            {
                return intro.ToIntroString(255);
            }

            if (content.Property["MainBody"]?.Value is XhtmlString mainBody)
            {
                return HttpUtility.HtmlDecode(mainBody.ToHtmlString()).StripHtml().ToIntroString(255);
            }

            return string.Empty;
        }
    }
}