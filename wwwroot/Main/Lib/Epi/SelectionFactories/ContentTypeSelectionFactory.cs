﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EPiServer.DataAbstraction;
using EPiServer.ServiceLocation;
using EPiServer.Shell.ObjectEditing;

namespace Furb.Main.Lib.Epi.SelectionFactories
{
    [SelectionFactoryRegistration]
    public class ContentTypeSelectionFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            var contentTypeRepository = ServiceLocator.Current.GetInstance<IContentTypeRepository>();
            var contentTypes = contentTypeRepository.List().Where(contentType => contentType.IsAvailable).ToDictionary(x => x.ID, x => "[" + x.LocalizedGroupName + "] " + x.LocalizedName);
            return contentTypes.OrderBy(x => x.Value).Select(x => new SelectItem {Text = x.Value, Value = x.Key.ToString(CultureInfo.InvariantCulture)});
        }
    }
}