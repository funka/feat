﻿using System;
using System.Collections.Generic;
using EPiServer.Framework.Localization;
using EPiServer.Shell.ObjectEditing;

namespace Furb.Main.Lib.Epi.SelectionFactories
{
    public class EnumSelectionFactory<TEnum> : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            var values = Enum.GetValues(typeof(TEnum));
            foreach (var value in values)
            {
                yield return new SelectItem
                {
                    Text = GetValueName(value),
                    Value = value
                };
            }
        }

        private static string GetValueName(object value)
        {
            var staticName = Enum.GetName(typeof(TEnum), value);

            if (staticName != null)
            {
                var localizationPath = $"/property/enum/{typeof(TEnum).Name.ToLowerInvariant()}/{staticName.ToLowerInvariant()}";

                if (LocalizationService.Current.TryGetString(localizationPath, out var localizedName))
                {
                    return localizedName;
                }
            }

            return staticName;
        }
    }
}