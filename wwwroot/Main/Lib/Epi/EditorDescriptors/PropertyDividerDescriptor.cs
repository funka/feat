﻿using System;
using System.Collections.Generic;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;

namespace Furb.Main.Lib.Epi.EditorDescriptors
{
    [EditorDescriptorRegistration(TargetType = typeof(string), UIHint = "propertydivider")]
    public class PropertyDividerDescriptor : EditorDescriptor
    {
        public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            ClientEditingClass = "funka.editors.propertydivider";
            EditorConfiguration["displayname"] = metadata.DisplayName;
            EditorConfiguration["description"] = metadata.Description;
            base.ModifyMetadata(metadata, attributes);
        }
    }
}