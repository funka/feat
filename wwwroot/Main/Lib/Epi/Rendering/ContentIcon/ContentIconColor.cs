﻿namespace Furb.Main.Lib.Epi.Rendering.ContentIcon
{
    public enum ContentIconColor
    {
        Default,
        Inverted,
        Active,
        Success,
        Danger,
        Warning,
        Info
    }
}
