﻿using System;
using System.IO;
using System.Linq;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using Furb.Main.Lib.Epi.Rendering.FeatureEngine.ViewEngines;

namespace Furb.Main.Lib.Epi.Rendering.FeatureEngine.Initialization
{
    [InitializableModule]
    public class FeaturerInitializationModule : IInitializableModule
    {
        private bool initiliazed;

        public void Initialize(InitializationEngine context)
        {
            if (initiliazed)
            {
                return;
            }

            var basedir = AppDomain.CurrentDomain.BaseDirectory;
            var dirs = Directory.GetDirectories(basedir);

            var isFeaturePattern = dirs.Any(x =>
            {
                var folderName = new DirectoryInfo(x).Name;
                return !string.IsNullOrWhiteSpace(folderName) && folderName.Equals("Features");
            });

            if (!isFeaturePattern)
            {
                return;
            }

            System.Web.Mvc.ViewEngines.Engines.Add(new LayoutViewEngine());
            System.Web.Mvc.ViewEngines.Engines.Add(new FeaturesLayoutViewEngine());
            initiliazed = true;
        }

        public void Uninitialize(InitializationEngine context)
        {
        }
    }
}
