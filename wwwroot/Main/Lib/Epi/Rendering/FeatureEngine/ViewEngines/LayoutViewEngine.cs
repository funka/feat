﻿using System.Web.Mvc;

namespace Furb.Main.Lib.Epi.Rendering.FeatureEngine.ViewEngines
{
    public class LayoutViewEngine : RazorViewEngine
    {
        public LayoutViewEngine()
        {
            var viewLocations = new[] { "~/Main/Views/Shared/{0}.cshtml",
                "~/Main/Views/MetaData/{0}.cshtml",
                "~/Main/Views/Shared/Layouts/{0}.cshtml",
                "~/Main/Views/Shared/PagePartials/{0}.cshtml",
                "~/Main/Views/Shared/EditorTemplates/{0}.cshtml",
                "~/Main/Views/Shared/DisplayTemplates/{0}.cshtml",
            };
            PartialViewLocationFormats = viewLocations;
            ViewLocationFormats = viewLocations;
        }
    }
}
