﻿using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;
using EPiServer.Web;

namespace Furb.Main.Lib.Epi.Rendering
{
    [InitializableModule]
    public class TemplateResolverEventHandler : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            ServiceLocator.Current.GetInstance<TemplateResolver>().TemplateResolving += TemplateCoordinatorInitialized.OnTemplateResolving;
        }

        public void Uninitialize(InitializationEngine context)
        {
            ServiceLocator.Current.GetInstance<TemplateResolver>().TemplateResolving -= TemplateCoordinatorInitialized.OnTemplateResolving;
        }
    }
}
