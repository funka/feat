﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using EPiServer.Framework.Localization;
using Furb.Main.Lib.Epi.Adapters;

namespace Furb.Main.Lib.Epi.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class LocalizedRegularExpressionAttribute : RegularExpressionAttribute, IClientValidatable
    {
        private const string EmailRegex = @"^[a-zA-Z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&\'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";

        private const string ImageRegex = @"^([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif)$";

        private const string SwedishSocialNumber = "^(19|20)?[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[- ]?[0-9]{4}$";

        private const string PasswordComplex = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{4,}$";

        private const string PasswordMedium = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{3,}$";

        public LocalizedRegularExpressionAttribute(string pattern) : base(CheckPattern(pattern))
        {
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(LocalizedRegularExpressionAttribute), typeof(LocalizedRegularExpressionAdapter));
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            return new[] {new ModelClientValidationRegexRule(ErrorMessage, Pattern)};
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(CultureInfo.CurrentCulture, LocalizationService.Current.GetString(ErrorMessageString), name, Pattern);
        }

        private static string CheckPattern(string pattern)
        {
            switch (pattern.ToLower().Trim())
            {
                case "email":
                    return EmailRegex;
                case "image":
                    return ImageRegex;
                case "swedishsocialnumber":
                    return SwedishSocialNumber;
                case "passwordcomplex":
                    return PasswordComplex;
                case "passwordmedium":
                    return PasswordMedium;
                default:
                    return pattern;
            }
        }
    }
}