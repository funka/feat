﻿using System.ComponentModel;
using EPiServer.Framework.Localization;

namespace Furb.Main.Lib.Epi.Attributes
{
    public class LocalizedDisplayNameAttribute : DisplayNameAttribute
    {
        private readonly string resourceName;

        public LocalizedDisplayNameAttribute(string resourceName)
        {
            this.resourceName = resourceName;
        }

        public override string DisplayName => resourceName.StartsWith("/") ? LocalizationService.Current.GetString(resourceName) : resourceName;
    }
}