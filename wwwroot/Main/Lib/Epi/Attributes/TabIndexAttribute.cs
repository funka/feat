﻿using System;
using System.Web.Mvc;

namespace Furb.Main.Lib.Epi.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]

    public class TabIndexAttribute : Attribute, IMetadataAware
    {
        private readonly int index;

        public TabIndexAttribute(int index)
        {
            this.index = index;
        }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            metadata.AdditionalValues.Add("TabIndex", index);
        }
    }
}