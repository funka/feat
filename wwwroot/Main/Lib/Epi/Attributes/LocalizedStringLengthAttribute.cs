﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using EPiServer.Framework.Localization;
using Furb.Main.Lib.Epi.Adapters;

namespace Furb.Main.Lib.Epi.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class LocalizedStringLengthAttribute : StringLengthAttribute
    {
        public LocalizedStringLengthAttribute(int maximumLength) : base(maximumLength)
        {
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(LocalizedStringLengthAttribute), typeof(LocalizedStringLengthAdapter));
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            return new[] {new ModelClientValidationStringLengthRule(ErrorMessage, MinimumLength, MaximumLength)};
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(LocalizationService.Current.GetString(ErrorMessage), name, MaximumLength, MinimumLength);
        }
    }
}