﻿using System.Collections.Generic;
using EPiServer.Core;

namespace Furb.Main.Lib.Epi.Sorting
{
    public sealed class AlphabeticalPageComparer : IComparer<PageData>
    {
        public int Compare(PageData x, PageData y)
        {
            return string.Compare(x.PageName, y.PageName, System.StringComparison.Ordinal);
        }
    }
}