﻿using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using Furb.Main.Lib.MobileMenu.Interfaces;

namespace Furb.Main.Lib.MobileMenu
{
    public static class ContentEvents
    {
        public static void ClearMobileMenuCache(object sender, DeleteContentEventArgs e)
        {
            MobileMenuCache.ClearMobileMenuCache();
        }

        public static void ClearMobileMenuCache(object sender, ContentEventArgs e)
        {
            MobileMenuCache.ClearMobileMenuCache();
        }

        public static void ClearMobileMenuCacheOnlyIfPageIsChanged(object sender, ContentEventArgs e)
        {
            if (e.Content == null || e.ContentLink == null)
            {
                return;
            }

            var oldPageExists = ServiceLocator.Current.GetInstance<IContentLoader>().TryGet(new ContentReference(e.ContentLink.ID), out PageData oldPage);

            if (!oldPageExists)
            {
                return;
            }
            if (oldPage is IHideInMenuWhenAuthenticated)
            {
                MobileMenuCache.ClearMobileMenuCache();
                return;
            }

            var newPage = e.Content as PageData;

            if (newPage == null)
            {
                return;
            }

            if (oldPage.PageName != newPage.PageName || oldPage.URLSegment != newPage.URLSegment || oldPage.VisibleInMenu != newPage.VisibleInMenu)
            {
                MobileMenuCache.ClearMobileMenuCache();
            }
        }
    }
}