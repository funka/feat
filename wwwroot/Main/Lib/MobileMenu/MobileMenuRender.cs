﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using Furb.Main.Lib.MobileMenu.Models;

namespace Furb.Main.Lib.MobileMenu
{
    public static class MobileMenuRender
    {
        public static string RenderMenuList(Dictionary<int, MobileMenuItem> items, int currentPageId, string nextLevelText)
        {
            try
            {
                var startPoint = MobileMenuCache.GetStartPoint(currentPageId);
                var currentPathList = GetCurrentPathListWithoutRootPage(startPoint, items, currentPageId);
                var menuList = GetMenuList(currentPathList.ToList(), currentPageId, items, nextLevelText);

                return GetHtmlMarkup(menuList);
            }
            catch (Exception exception)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(exception, HttpContext.Current);

                return string.Empty;
            }
        }

        private static HtmlGenericControl CreateDiv(string className)
        {
            var divRow = new HtmlGenericControl("div");
            divRow.Attributes.Add("class", className);
            return divRow;
        }

        private static HtmlGenericControl CreateLi(int currentPageId, MobileMenuItem item, bool hasLoadedChildren)
        {
            var li = new HtmlGenericControl("li");

            li.Attributes.Add("class", "menuItem");

            if (item.Id == currentPageId)
            {
                li.Attributes["class"] += " active";
            }

            if (hasLoadedChildren)
            {
                li.Attributes["class"] += " hasLoadedChildren";
            }

            li.Attributes.Add("data-id", item.Id.ToString(CultureInfo.InvariantCulture));

            return li;
        }

        private static HtmlGenericControl CreateLink(MobileMenuItem item)
        {
            var link = new HtmlGenericControl("a")
            {
                InnerHtml = item.Name
            };

            link.Attributes.Add("href", item.Url);
            link.Attributes.Add("class", "small-" + (item.Children.Any() ? "10" : "12") + " columns menuLink");

            return link;
        }

        private static HtmlGenericControl CreateNextButton(ICollection<int> currentPathList, string nextLevelText, MobileMenuItem item)
        {
            var nextButton = new HtmlGenericControl("button")
            {
                InnerText = nextLevelText + " " + item.Name
            };

            nextButton.Attributes.Add("data-href", item.Url);
            nextButton.Attributes.Add("class", "small-2 columns menuLink haveLinks hideForNoscript");
            nextButton.Attributes.Add("aria-expanded", currentPathList.Contains(item.Id).ToString().ToLower());

            return nextButton;
        }

        private static IEnumerable<int> GetCurrentPathListWithoutRootPage(ContentReference startPoint, IReadOnlyDictionary<int, MobileMenuItem> items, int currentPageId)
        {
            var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();

            var currentPathList = items[currentPageId] != null ? items[currentPageId].Path : null;

            while (currentPathList == null)
            {
                var page = contentLoader.Get<PageData>(new ContentReference(currentPageId));
                currentPageId = page.ParentLink.ID;
                currentPathList = items[currentPageId] != null ? items[currentPageId].Path : null;
            }

            currentPathList = startPoint == ContentReference.RootPage ? currentPathList.Where(item => item != ContentReference.RootPage.ID).Reverse() : currentPathList.Reverse();

            return currentPathList;
        }

        private static string GetHtmlMarkup(Control htmlGenericControl)
        {
            var stringbuilder = new StringBuilder();
            var htmlTextWriter = new HtmlTextWriter(new StringWriter(stringbuilder));
            htmlGenericControl.RenderControl(htmlTextWriter);

            return stringbuilder.ToString();
        }

        private static HtmlGenericControl GetMenuList(ICollection<int> currentPathList, int currentPageId, IReadOnlyDictionary<int, MobileMenuItem> mobileMenuItems, string nextLevelText)
        {
            var list = new HtmlGenericControl("ul");

            var currentPath = currentPathList.FirstOrDefault();
            var currentItem = mobileMenuItems[currentPath];

            foreach (var childrenId in currentItem.Children)
            {
                var item = mobileMenuItems[childrenId];

                if (item != null)
                {
                    var hasChildrenInActivePath = item.Children.Any() && currentPathList.Contains(item.Id);
                    var li = CreateLi(currentPageId, item, hasChildrenInActivePath);
                    var div = CreateDiv("row");
                    var childrenDiv = CreateDiv("childrenListContainer");

                    div.Controls.Add(CreateLink(item));

                    if (item.Children.Any())
                    {
                        div.Controls.Add(CreateNextButton(currentPathList, nextLevelText, item));
                    }

                    li.Controls.Add(div);

                    if (hasChildrenInActivePath)
                    {
                        childrenDiv.Controls.Add(GetMenuList(currentPathList.Skip(1).ToList(), currentPageId, mobileMenuItems, nextLevelText));
                    }

                    li.Controls.Add(childrenDiv);

                    list.Controls.Add(li);
                }
            }

            return list;
        }
    }
}