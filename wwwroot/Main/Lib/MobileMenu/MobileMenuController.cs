﻿using System.Collections.Generic;
using System.Web.Http;
using Furb.Main.Lib.MobileMenu.Interfaces;
using Furb.Main.Lib.MobileMenu.Models;

namespace Furb.Main.Lib.MobileMenu
{
    public class MobileMenuController : ApiController
    {
        public Dictionary<int, MobileMenuItem> GetAllMobileMenus(int currentId, string lang)
        {
            return MobileMenuCache.GetMobileMenuCachedList(typeof(IMobileSitePageData), currentId, lang);
        }
    }
}
