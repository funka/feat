﻿using EPiServer.Core;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;

namespace Furb.Main.Lib.MobileMenu
{
    [InitializableModule]
    public class EventInitializationModule : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            var eventRegistry = ServiceLocator.Current.GetInstance<IContentEvents>();

            eventRegistry.DeletedContent += ContentEvents.ClearMobileMenuCache;
            eventRegistry.MovedContent += ContentEvents.ClearMobileMenuCache;
            eventRegistry.PublishingContent += ContentEvents.ClearMobileMenuCacheOnlyIfPageIsChanged;
        }

        public void Uninitialize(InitializationEngine context)
        {
            var eventRegistry = ServiceLocator.Current.GetInstance<IContentEvents>();

            eventRegistry.DeletedContent -= ContentEvents.ClearMobileMenuCache;
            eventRegistry.MovedContent -= ContentEvents.ClearMobileMenuCache;
            eventRegistry.PublishingContent -= ContentEvents.ClearMobileMenuCacheOnlyIfPageIsChanged;
        }
    }
}