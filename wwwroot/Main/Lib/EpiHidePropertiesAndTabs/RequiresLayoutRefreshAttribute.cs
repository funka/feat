using System;

namespace Furb.Main.Lib.EpiHidePropertiesAndTabs
{
    [AttributeUsage(AttributeTargets.Property)]
    public class RequiresLayoutRefreshAttribute : Attribute
    {
    }
}