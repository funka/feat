using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Shell.ObjectEditing;
using Furb.Main.Lib.EpiHidePropertiesAndTabs.LayoutVisibilityResolver;
using Furb.Main.Lib.EpiHidePropertiesAndTabs.VisibilityAttributes;

namespace Furb.Main.Lib.EpiHidePropertiesAndTabs
{
    [ServiceConfiguration(IncludeServiceAccessor = false)]
    public class HiddenPropertiesMetadataExtender : IMetadataExtender
    {
        private readonly ILayoutVisibilityResolver layoutVisibilityResolver;

        public HiddenPropertiesMetadataExtender(CompositeLayoutResolver layoutVisibilityResolver)
        {
            this.layoutVisibilityResolver = layoutVisibilityResolver;
        }

        public void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            var content = metadata.Model as IContent;
            if (content == null)
            {
                return;
            }

            metadata.CustomEditorSettings["hiddenTabs"] = layoutVisibilityResolver.GetHiddenTabs(content);
            metadata.CustomEditorSettings["hiddenProperties"] = layoutVisibilityResolver.GetHiddenProperties(content);

            foreach (var metadataProperty in metadata.Properties.Cast<ExtendedMetadata>())
            {
                if (metadataProperty.Attributes.OfType<RequiresLayoutRefreshAttribute>().Any() || metadataProperty.Attributes.OfType<ILayoutElementVisibilityAttribute>().Any())
                {
                    metadataProperty.CustomEditorSettings["requiresLayoutRefresh"] = true;
                }

                if (!metadataProperty.Properties.Any())
                {
                    continue;
                }

                foreach (var subMetadataProperty in metadataProperty.Properties.Cast<ExtendedMetadata>())
                {
                    if (subMetadataProperty.Attributes.OfType<RequiresLayoutRefreshAttribute>().Any() || subMetadataProperty.Attributes.OfType<ILayoutElementVisibilityAttribute>().Any())
                    {
                        subMetadataProperty.CustomEditorSettings["requiresLayoutRefresh"] = true;
                    }

                    foreach (var subSubMetadataProperty in subMetadataProperty.Properties.Cast<ExtendedMetadata>())
                    {
                        if (subSubMetadataProperty.Attributes.OfType<RequiresLayoutRefreshAttribute>().Any() || subSubMetadataProperty.Attributes.OfType<ILayoutElementVisibilityAttribute>().Any())
                        {
                            subSubMetadataProperty.CustomEditorSettings["requiresLayoutRefresh"] = true;
                        }
                    }
                }
            }
        }
    }
}