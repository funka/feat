using System;

namespace Furb.Main.Lib.EpiHidePropertiesAndTabs.VisibilityAttributes
{
    public abstract class PropertyVisibilityAttributeBase : Attribute, ILayoutElementVisibilityAttribute
    {
        protected PropertyVisibilityAttributeBase(string propertyName, object value)
        {
            PropertyName = propertyName;
            Value = value;
        }

        public string PropertyName { get; set; }

        public object Value { get; set; }
    }
}