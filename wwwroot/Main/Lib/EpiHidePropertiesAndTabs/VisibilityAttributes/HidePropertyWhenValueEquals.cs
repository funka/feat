using System;

namespace Furb.Main.Lib.EpiHidePropertiesAndTabs.VisibilityAttributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class HidePropertyWhenValueEquals : PropertyVisibilityAttributeBase
    {
        public HidePropertyWhenValueEquals(string propertyName, object value) : base(propertyName, value)
        {
        }
    }
}