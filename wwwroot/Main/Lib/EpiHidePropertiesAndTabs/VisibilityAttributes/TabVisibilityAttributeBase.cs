using System;

namespace Furb.Main.Lib.EpiHidePropertiesAndTabs.VisibilityAttributes
{
    public abstract class TabVisibilityAttributeBase : Attribute, ILayoutElementVisibilityAttribute
    {
        protected TabVisibilityAttributeBase(string tabName, object value)
        {
            TabName = tabName;
            Value = value;
        }

        public string TabName { get; set; }

        public object Value { get; set; }
    }
}