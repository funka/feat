using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.ServiceLocation;

namespace Furb.Main.Lib.EpiHidePropertiesAndTabs.LayoutVisibilityResolver
{
    [ServiceConfiguration(typeof(CompositeLayoutResolver))]
    public class CompositeLayoutResolver : ILayoutVisibilityResolver
    {
        private readonly IEnumerable<ILayoutVisibilityResolver> layoutVisibilityResolvers;

        public CompositeLayoutResolver(IEnumerable<ILayoutVisibilityResolver> layoutVisibilityResolvers)
        {
            this.layoutVisibilityResolvers = layoutVisibilityResolvers;
        }

        public IEnumerable<string> GetHiddenTabs(IContent content)
        {
            return layoutVisibilityResolvers.SelectMany(x => x.GetHiddenTabs(content)).Distinct();
        }

        public IEnumerable<string> GetHiddenProperties(IContent content)
        {
            return layoutVisibilityResolvers.SelectMany(x => x.GetHiddenProperties(content)).Distinct();
        }
    }
}