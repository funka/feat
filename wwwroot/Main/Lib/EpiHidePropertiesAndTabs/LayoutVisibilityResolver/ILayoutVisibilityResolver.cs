using System.Collections.Generic;
using EPiServer.Core;

namespace Furb.Main.Lib.EpiHidePropertiesAndTabs.LayoutVisibilityResolver
{
    public interface ILayoutVisibilityResolver
    {
        IEnumerable<string> GetHiddenTabs(IContent content);

        IEnumerable<string> GetHiddenProperties(IContent content);
    }
}
