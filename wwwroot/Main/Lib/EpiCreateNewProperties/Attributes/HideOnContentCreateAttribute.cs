﻿using System;

namespace Furb.Main.Lib.EpiCreateNewProperties.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class HideOnContentCreateAttribute : Attribute
    {
    }
}
