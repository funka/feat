﻿using System;
using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.Shell.ObjectEditing;
using Furb.Main.Lib.EpiCreateNewProperties.Attributes;

namespace Furb.Main.Lib.EpiCreateNewProperties
{
    public class ContentCreateMetadataExtender : IMetadataExtender
    {
        public void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            foreach (var modelMetadata in metadata.Properties)
            {
                var property = (ExtendedMetadata) modelMetadata;

                property.EditorConfiguration["displayOnContentCreate"] = property.Attributes.OfType<DisplayOnContentCreateAttribute>().Any();
                property.EditorConfiguration["hideOnContentCreate"] = property.Attributes.OfType<HideOnContentCreateAttribute>().Any();
            }

            if (metadata.Model is IContent contentCreate && contentCreate.ContentLink.ID == 0)
            {
                foreach (var modelMetadata in metadata.Properties)
                {
                    var property = (ExtendedMetadata)modelMetadata;

                    if (property.Attributes.OfType<HideOnContentCreateAttribute>().Any())
                    {
                        property.IsRequired = false;
                    }
                }
            }
        }
    }
}