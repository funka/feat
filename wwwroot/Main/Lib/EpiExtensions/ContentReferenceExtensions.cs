﻿using System;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using Furb.Main.Lib.EpiExtensions.Interfaces;

namespace Furb.Main.Lib.EpiExtensions
{
    public static class ContentReferenceExtensions
    {
        public static PageData GetPage(this ContentReference contentReference)
        {
            return contentReference.GetPage<PageData>();
        }

        public static T GetPage<T>(this ContentReference contentReference) where T : PageData
        {
            if (contentReference.IsNullOrEmpty())
            {
                return null;
            }

            var loader = ServiceLocator.Current.GetInstance<IContentLoader>();
            return loader.Get<PageData>(contentReference) as T;
        }

        public static bool IsNullOrEmpty(this ContentReference contentReference)
        {
            return ContentReference.IsNullOrEmpty(contentReference);
        }

        public static T GetStartPage<T>(this ContentReference pageLink) where T : IStartPage
        {
            if (pageLink == null)
            {
                throw new ArgumentNullException(nameof(pageLink), "missing content reference to get start page for");
            }

            var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();
            var siteStartPage = (T)contentLoader.Get<IStartPage>(ContentReference.StartPage);
            IStartPage pageLinkAsPage;

            try
            {
                pageLinkAsPage = pageLink.ToPageReference().GetPage() as IStartPage;
            }
            catch (Exception)
            {
                return siteStartPage;
            }

            var pageLinkAsStartPage = pageLinkAsPage;
            if (pageLinkAsStartPage != null)
            {
                return (T)pageLinkAsStartPage;
            }

            var startPageAncestor = contentLoader.GetAncestors(pageLink).FirstOrDefault(p => p is IStartPage);
            return startPageAncestor == null ? siteStartPage : contentLoader.Get<T>(startPageAncestor.ContentLink);
        }
    }
}