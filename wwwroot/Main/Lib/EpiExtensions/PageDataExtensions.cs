﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;

namespace Furb.Main.Lib.EpiExtensions
{
    public static class PageDataExtensions
    {
        public static PageDataCollection GetChildren(this PageData page)
        {
            return page.GetChildren<PageData>().ToPageDataCollection();
        }

        public static PageDataCollection GetChildren(this PageData page, CultureInfo lang)
        {
            return page.GetChildren<PageData>(lang).ToPageDataCollection();
        }

        public static IEnumerable<T> GetChildren<T>(this PageData page)
            where T : PageData
        {
            if (page == null)
            {
                return Enumerable.Empty<T>();
            }
            var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();
            return contentLoader.GetChildren<T>(page.ContentLink);
        }

        public static IEnumerable<T> GetChildren<T>(this PageData page, CultureInfo lang)
            where T : PageData
        {
            if (page == null)
            {
                return Enumerable.Empty<T>();
            }

            var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();

            return contentLoader.GetChildren<T>(page.ContentLink, lang);
        }

        public static PageData GetParent(this PageData page)
        {
            if (page == null || PageReference.IsNullOrEmpty(page.ParentLink) || DataFactory.Instance.IsWastebasket(page.PageLink))
            {
                return null;
            }

            return page.ParentLink.GetPage();
        }

        public static PageDataCollection ToPageDataCollection(this IEnumerable<PageData> pages)
        {
            return new PageDataCollection(pages);
        }

        public static IEnumerable<PageData> GetAncestors(this PageData page)
        {
            return page.GetAncestors(ContentReference.RootPage);
        }

        public static IEnumerable<PageData> GetAncestors(this PageData page, PageReference rootPage)
        {
            var parent = page.GetParent();

            if (parent != null)
            {
                yield return parent;

                if (parent.PageLink.ID != rootPage.ID)
                {
                    foreach (var ancestor in parent.GetAncestors(rootPage))
                    {
                        yield return ancestor;
                    }
                }
            }
        }

        public static T GetProperty<T>(this IContent page)
        {
            foreach (var propertyData in page.Property)
            {
                if (propertyData.Value is T value)
                {
                    return value;
                }
            }

            return default;
        }
    }
}