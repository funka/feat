﻿using EPiServer.Core;

namespace Furb.Main.Lib.EpiExtensions.Interfaces
{
    public interface IStartPage : IContentData
    {
    }
}
