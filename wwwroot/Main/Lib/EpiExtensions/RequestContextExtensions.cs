﻿using System.Web.Routing;
using EPiServer.Core;
using EPiServer.Globalization;
using EPiServer.Web.Routing;

namespace Furb.Main.Lib.EpiExtensions
{
    public static class RequestContextExtensions
    {
        public static RouteValueDictionary GetPageRoute(this RequestContext requestContext, PageReference pageLink)
        {
            var values = new RouteValueDictionary
            {
                [RoutingConstants.NodeKey] = pageLink,
                [RoutingConstants.LanguageKey] = ContentLanguage.PreferredCulture.Name
            };

            var idkeep = requestContext.HttpContext.Request.QueryString["idkeep"];

            if (idkeep != null)
            {
                values["id"] = pageLink.ToString();
            }

            return values;
        }
    }
}