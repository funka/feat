﻿using System.Collections.Generic;
using System.Linq;
using EPiServer;
using EPiServer.Core;

namespace Furb.Main.Lib.EpiExtensions
{
    public static class ContentLoaderExtensions
    {
        public static IEnumerable<T> GetDescendantsOfType<T>(this IContentLoader contentLoader, ContentReference contentLink) where T : IContent
        {
            if (contentLoader == null || contentLink == null)
            {
                return Enumerable.Empty<T>();
            }

            return contentLoader.GetDescendents(contentLink).Select(contentLoader.Get<IContent>).Where(d => d is T).Cast<T>();
        }
    }
}