﻿using System.Collections.Generic;
using System.Linq;
using EPiServer.Core;
using EPiServer.Filters;
using EPiServer.Framework.Web;
using EPiServer.ServiceLocation;

namespace Furb.Main.Lib.EpiExtensions
{
    public static class ContentExtensions
    {
        public static IEnumerable<T> FilterForDisplay<T>(this IEnumerable<T> content, bool requirePageTemplate = false, bool requireVisibleInMenu = false)
            where T : IContent
        {
            if (content == null)
            {
                return Enumerable.Empty<T>();
            }

            var publishedFilter = new FilterPublished();
            var accessFilter = new FilterAccess();
            content = content.Where(x => !publishedFilter.ShouldFilter(x) && !accessFilter.ShouldFilter(x));

            if (requirePageTemplate)
            {
                var templateFilter = ServiceLocator.Current.GetInstance<FilterTemplate>();
                templateFilter.TemplateTypeCategories = TemplateTypeCategories.Page;
                content = content.Where(x => !templateFilter.ShouldFilter(x));
            }

            if (requireVisibleInMenu)
            {
                content = content.Where(x => VisibleInMenu(x));
            }

            return content;
        }

        private static bool VisibleInMenu(IContent content)
        {
            return !(content is PageData page) || page.VisibleInMenu;
        }
    }
}