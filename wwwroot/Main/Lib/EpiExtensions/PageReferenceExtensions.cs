﻿using System.Collections.Generic;
using EPiServer.Core;

namespace Furb.Main.Lib.EpiExtensions
{
    public static class PageReferenceExtensions
    {
        public static bool IsNullOrEmpty(this PageReference pageReference)
        {
            return PageReference.IsNullOrEmpty(pageReference);
        }

        public static PageDataCollection ToPageDataCollection(this IEnumerable<PageReference> pageReferences)
        {
            return pageReferences.ToPageDataCollection();
        }
    }
}