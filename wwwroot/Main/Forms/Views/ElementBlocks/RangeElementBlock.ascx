<%--
    ====================================
    Version: 4.28.0.0. Modified: 20200323
    ====================================
--%>

<%@ Import Namespace="System.Web.Mvc" %>
<%@ Import Namespace="EPiServer.Forms.Helpers.Internal" %>
<%@ Import Namespace="EPiServer.Forms.Implementation.Elements" %>

<%@ Control Language="C#" Inherits="ViewUserControl<RangeElementBlock>" %>

<%
    var formElement = Model.FormElement;
    var labelText = Model.Label;
    var cssClasses = Model.GetValidationCssClasses();
%>

<% using(Html.BeginElement(Model, new { @class="FormRange" + cssClasses, data_f_type="range" })) { %>
    <label for="<%: formElement.Guid %>" class="Form__Element__Caption"><%:labelText %></label>
    <span>
        <span class="FormRange__Min"><%: Model.Min %></span>
        <input name="<%: formElement.ElementName %>" id="<%: formElement.Guid %>" type="range" class="FormRange__Input"
               value="<%: Model.GetRangeValue() %>" data-f-datainput
               min="<%: Model.Min %>" max="<%: Model.Max %>" step="<%: Model.Step %>" <%= Model.AttributesString %> aria-describedby="<%: Util.GetAriaDescribedByElementName(formElement.ElementName) %>"
               aria-invalid="<%: Util.GetAriaInvalidByValidationCssClasses(cssClasses) %>" />
        <span class="FormRange__Max"><%: Model.Max %></span>
    </span>
    <span id="<%: Util.GetAriaDescribedByElementName(formElement.ElementName) %>" data-f-linked-name="<%: formElement.ElementName %>" data-f-validationerror class="Form__Element__ValidationError" style="display: none;">*</span>
<% } %>
