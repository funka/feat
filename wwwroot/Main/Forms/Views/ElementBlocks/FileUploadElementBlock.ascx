<%--
    ====================================
    Version: 4.28.0.0. Modified: 20200220
    ====================================
--%>

<%@ import namespace="System.Web.Mvc" %>
<%@ Import Namespace="EPiServer.Shell.Web.Mvc.Html" %>
<%@ import namespace="EPiServer.Forms.Helpers.Internal" %>
<%@ import namespace="EPiServer.Forms.Implementation.Elements" %>
<%@ control language="C#" inherits="ViewUserControl<FileUploadElementBlock>" %>

<%  var formElement = Model.FormElement; 
    var labelText = Model.Label;
    var allowMultiple = Model.AllowMultiple ? "multiple" : string.Empty;
    var cssClasses = Model.GetValidationCssClasses();
%>

<% using(Html.BeginElement(Model, new { @class="FormFileUpload" + cssClasses, data_f_type="fileupload" })) { %>
    <label for="<%: formElement.Guid %>" class="Form__Element__Caption"><%: labelText %></label>
    <input name="<%: formElement.ElementName %>" id="<%: formElement.Guid %>" type="file" class="FormFileUpload__Input" <%:allowMultiple%>
        <% if(!string.IsNullOrEmpty(Model.FileExtensions)) { %>
			accept="<%=Model.FileExtensions%>"
    	<%}%>     
        <%= Model.AttributesString %> data-f-datainput aria-describedby="<%: Util.GetAriaDescribedByElementName(formElement.ElementName) %>"
            aria-invalid="<%: Util.GetAriaInvalidByValidationCssClasses(cssClasses) %>" />
    <div class="FormFileUpload__PostedFile" data-f-postedFile></div>
    <%= Html.ValidationMessageFor(Model) %>
<% } %>
