<%--
    ====================================
    Version: 4.28.0.0. Modified: 20200323
    ====================================
--%>

<%@ Import Namespace="System.Web.Mvc" %>
<%@ Import Namespace="EPiServer.Forms.Implementation.Elements" %>
<%@ import namespace="EPiServer.Forms.Helpers.Internal" %>
<%@ Control Language="C#" Inherits="ViewUserControl<SubmitButtonElementBlock>" %>

<%
    var formElement = Model.FormElement;
    var buttonText = Model.Label;
    var buttonDisableState = Model.GetFormSubmittableStatus(ViewContext.Controller.ControllerContext.HttpContext);
    var cssClasses = Model.GetValidationCssClasses();
%>

<button id="<%: formElement.Guid %>" name="submit" type="submit" value="<%: formElement.Guid %>" data-f-is-finalized="<%: Model.FinalizeForm.ToString().ToLower() %>"
    data-f-is-progressive-submit="true" data-f-type="submitbutton" data-f-element-name="<%: formElement.ElementName %>"
    <%= Model.AttributesString %> <%: buttonDisableState %>
    <% if (Model.Image == null) 
    { %>
        class="Form__Element FormExcludeDataRebind FormSubmitButton<%: cssClasses %>">
        <%: buttonText %>
    <% } else { %>
        class="Form__Element FormExcludeDataRebind FormSubmitButton FormImageSubmitButton<%: cssClasses %>">
        <img src="<%: Model.Image.Path %>" data-f-is-progressive-submit="true" data-f-is-finalized="<%: Model.FinalizeForm.ToString().ToLower() %>" alt="<%: buttonText %>"/>
    <% } %>
</button>
