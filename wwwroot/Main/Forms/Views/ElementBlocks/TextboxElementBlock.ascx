<%--
    =====================================
    Version: 4.28.0.0. Modified: 20200226
    =====================================
--%>

<%@ import namespace="System.Web.Mvc" %>
<%@ Import Namespace="EPiServer.Forms.Helpers.Internal" %>
<%@ import namespace="EPiServer.Forms.Implementation.Elements" %>
<%@ control language="C#" inherits="ViewUserControl<TextboxElementBlock>" %>

<%
    var formElement = Model.FormElement; 
    var labelText = Model.Label;
    var cssClasses = Model.GetValidationCssClasses();
%>

<% using(Html.BeginElement(Model, new { @class="FormTextbox" + cssClasses, data_f_type="textbox" })) { %>
    <label for="<%: formElement.Guid %>" class="Form__Element__Caption"><%: labelText %></label>
    <input name="<%: formElement.ElementName %>" id="<%: formElement.Guid %>" type="text" class="FormTextbox__Input" aria-describedby="<%: Util.GetAriaDescribedByElementName(formElement.ElementName) %>"
        <% if (!string.IsNullOrWhiteSpace(Model.PlaceHolder)) { %>
            placeholder="<%: Model.PlaceHolder %>"
        <% } %>
        value="<%: Model.GetDefaultValue() %>" <%= Model.AttributesString %> data-f-datainput
        aria-invalid="<%: Util.GetAriaInvalidByValidationCssClasses(cssClasses) %>" />
    <%= Html.ValidationMessageFor(Model) %>
    <%= Model.RenderDataList() %>
<% } %>
