<%--
    ====================================
    Version: 4.28.0.0. Modified: 20200220
    ====================================
--%>

<%@ import namespace="System.Web.Mvc" %>
<%@ import namespace="EPiServer.Forms.Helpers.Internal" %>
<%@ import namespace="EPiServer.Forms.Implementation.Elements" %>
<%@ control language="C#" inherits="ViewUserControl<ChoiceElementBlock>" %>

<%
    var formElement = Model.FormElement;
    var labelText = Model.Label;
    var items = Model.GetItems();
    var cssClasses = Model.GetValidationCssClasses();
%>

<% using(Html.BeginElement(Model, new { id=formElement.Guid, @class="FormChoice" + cssClasses, data_f_type="choice", aria_invalid=Util.GetAriaInvalidByValidationCssClasses(cssClasses) }, true)) { %>
<fieldset aria-describedby="<%: Util.GetAriaDescribedByElementName(formElement.ElementName) %>">
    <%  if(!string.IsNullOrEmpty(labelText)) { %>
    <legend class="Form__Element__Caption"><%: labelText %></legend>
    <% } 
        foreach (var item in items)
        {
            var defaultCheckedString = Model.GetDefaultSelectedString(item);
            var checkedString = string.IsNullOrEmpty(defaultCheckedString) ? string.Empty : "checked";
    %>

    <label>
        <% if(Model.AllowMultiSelect) { %>
        <input type="checkbox" name="<%: formElement.ElementName %>" value="<%: item.Value %>" class="FormChoice__Input FormChoice__Input--Checkbox" <%: checkedString %> <%: defaultCheckedString %> data-f-datainput />
        <% } else  { %>
        <input type="radio" name="<%: formElement.ElementName %>" value="<%: item.Value %>" class="FormChoice__Input FormChoice__Input--Radio" <%: checkedString %> <%: defaultCheckedString %> data-f-datainput />
        <% } %>
        <%: item.Caption %></label>

    <% } %>
</fieldset>
    <%= Html.ValidationMessageFor(Model) %>

<% } %>
