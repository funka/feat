﻿using System;
using EPiServer.Cms.TinyMce.Core;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;

namespace Furb.Main.Interfaces
{
    [ModuleDependency(typeof(TinyMceInitialization))]
    public class TinyMceSettingsInitialization : IConfigurableModule
    {
        public void ConfigureContainer(ServiceConfigurationContext context)
        {
            const string pluginList = "print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help code importcss spellchecker";

            var toolbarsNormal = new[]
            {
                "epi-link unlink image epi-image-editor media | epi-personalized-content | cut copy paste pastetext | table", "bold | bullist numlist hr | formatselect | styleselect removeformat | undo redo | searchreplace | code help"
            };

            context.Services.Configure<TinyMceConfiguration>(config =>
            {
                config.Default().AddSetting("importcss_append", false).ContentCss("/static/css/editor.css?" + DateTime.Now.Ticks).AddPlugin(pluginList).Toolbar(toolbarsNormal).BlockFormats("Paragraph=p;Header 2=h2;Header 3=h3;Header 4=h4;Header 5=h5;Header 6=h6;Quote=blockquote;");
            });
        }

        public void Initialize(InitializationEngine context)
        {
        }

        public void Uninitialize(InitializationEngine context)
        {
        }
    }
}