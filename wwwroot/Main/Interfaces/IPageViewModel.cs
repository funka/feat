﻿using EPiServer.Core;
using Furb.Features.Pages.Shared;

namespace Furb.Main.Interfaces
{
    public interface IPageViewModel<out T> where T : SitePageData
    {
        T CurrentPage { get; }

        LayoutModel Layout { get; set; }

        IContent Section { get; set; }
    }
}