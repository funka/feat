﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc.Html;
using EPiServer.Web.Routing;
using Furb.Features.Pages.Shared;
using Furb.Main.Lib.EpiExtensions;

namespace Furb.Main
{
    public static class HtmlHelpers
    {
        public static IHtmlString BreadCrumbList(this HtmlHelper helper, Func<BreadCrumbItem, HelperResult> itemTemplate = null, ContentReference pageReference = null)
        {
            itemTemplate = itemTemplate ?? GetBreadCrumbTemplate(helper);
            pageReference = pageReference ?? helper.ViewContext.RequestContext.GetContentLink();

            var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();

            var breadCrumbItem = CreateBreadCrumbItem(pageReference.GetPage(), pageReference, contentLoader);

            var buffer = new StringBuilder();
            var writer = new StringWriter(buffer);

            itemTemplate(breadCrumbItem).WriteTo(writer);

            return new MvcHtmlString(buffer.ToString());
        }

        public static string GetCssClass(MenuItem menuItem)
        {
            if (menuItem.SelectedAndCurrentPage)
            {
                return "selected";
            }

            return menuItem.Selected ? "active" : string.Empty;
        }

        public static IHtmlString MenuList(this HtmlHelper helper, ContentReference rootLink, Func<MenuItem, HelperResult> itemTemplate = null, bool includeRoot = false)
        {
            itemTemplate = itemTemplate ?? GetDefaultItemTemplate(helper);

            var currentContentLink = helper.ViewContext.RequestContext.GetContentLink();
            var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();

            Func<IEnumerable<PageData>, IEnumerable<PageData>> filter = pages => pages.FilterForDisplay(false, true);

            var pagePath = contentLoader.GetAncestors(currentContentLink ?? GetBackupLink())
                .Reverse()
                .Select(x => x.ContentLink)
                .SkipWhile(x => !x.CompareToIgnoreWorkID(rootLink))
                .ToList();

            var menuItems = contentLoader.GetChildren<PageData>(rootLink).FilterForDisplay(false, true)
                .Select(x => CreateMenuItem(x, currentContentLink ?? GetBackupLink(), pagePath, contentLoader, filter))
                .ToList();

            if (includeRoot)
            {
                menuItems.Insert(0, CreateMenuItem(contentLoader.Get<PageData>(rootLink), currentContentLink ?? GetBackupLink(), pagePath, contentLoader, filter));
            }

            var buffer = new StringBuilder();
            var writer = new StringWriter(buffer);

            foreach (var menuItem in menuItems)
            {
                itemTemplate(menuItem).WriteTo(writer);
            }

            return new MvcHtmlString(buffer.ToString());
        }

        private static ContentReference GetBackupLink()
        {
            throw new Exception("error!");
        }

        public static void RenderContentReference(this HtmlHelper html, ContentReference contentReference)
        {
            IContentData contentData = ServiceLocator.Current.GetInstance<IContentRepository>().Get<IContent>(contentReference);
            html.RenderContentData(contentData, false, html.ViewContext.ViewData["tag"] as string);
        }

        public static string TeaserLinkText(ContentPageData contentPage)
        {
            if (contentPage == null)
            {
                return string.Empty;
            }

            return string.IsNullOrEmpty(contentPage.TeaserBlock.LinkText) ? contentPage.PageName : contentPage.TeaserBlock.LinkText;
        }

        private static BreadCrumbItem CreateBreadCrumbItem(PageData page, ContentReference currentContentLink, IContentLoader contentLoader)
        {
            var breadCrumbItem = new BreadCrumbItem(page)
            {
                Ancestors = contentLoader.GetAncestors(currentContentLink).Where(x => x.ContentTypeID > 1).ToList()
            };

            breadCrumbItem.Ancestors.Reverse();

            return breadCrumbItem;
        }

        private static MenuItem CreateMenuItem(PageData page, ContentReference currentContentLink, List<ContentReference> pagePath, IContentLoader contentLoader, Func<IEnumerable<PageData>, IEnumerable<PageData>> filter)
        {
            var menuItem = new MenuItem(page)
            {
                Selected = page.ContentLink.CompareToIgnoreWorkID(currentContentLink) || pagePath.Contains(page.ContentLink),
                SelectedAndCurrentPage = page.ContentLink.CompareToIgnoreWorkID(currentContentLink) || (pagePath.Contains(page.ContentLink) && page.PageLink.ID == currentContentLink.ID),
                HasChildren = new Lazy<bool>(() => filter(contentLoader.GetChildren<PageData>(page.ContentLink)).Any())
            };

            return menuItem;
        }

        private static Func<BreadCrumbItem, HelperResult> GetBreadCrumbTemplate(HtmlHelper helper)
        {
            return x => new HelperResult(writer => writer.Write(helper.PageLink(x.Page)));
        }

        private static Func<MenuItem, HelperResult> GetDefaultItemTemplate(HtmlHelper helper)
        {
            return x => new HelperResult(writer => writer.Write(helper.PageLink(x.Page)));
        }

        public class BreadCrumbItem
        {
            public BreadCrumbItem(PageData page)
            {
                Page = page;
            }

            public List<IContent> Ancestors { get; set; }

            public PageData Page { get; set; }
        }

        public class ConditionalLink : IDisposable
        {
            private readonly bool linked;
            private readonly ViewContext viewContext;
            private bool disposed;

            public ConditionalLink(ViewContext viewContext, bool isLinked)
            {
                this.viewContext = viewContext;
                linked = isLinked;
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            protected virtual void Dispose(bool dispose)
            {
                if (disposed)
                {
                    return;
                }

                disposed = true;

                if (linked)
                {
                    viewContext.Writer.Write("</a>");
                }
            }
        }

        public class LinkSelector
        {
            public LinkSelector(string url, string[] listOfStrings)
            {
                Url = url;
                UrlSegments = listOfStrings;
            }

            public string Url { get; set; }

            public string[] UrlSegments { get; set; }
        }

        public class MenuItem
        {
            public MenuItem(PageData page)
            {
                Page = page;
            }

            public Lazy<bool> HasChildren { get; set; }

            public PageData Page { get; set; }

            public bool Selected { get; set; }

            public bool SelectedAndCurrentPage { get; set; }
        }
    }
}