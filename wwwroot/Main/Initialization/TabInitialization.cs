﻿using System;
using System.Linq;
using EPiServer.DataAbstraction;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.Security;
using EPiServer.ServiceLocation;

namespace Furb.Main.Initialization
{
    [InitializableModule]
    [ModuleDependency(typeof(EPiServer.Web.InitializationModule))]
    public class TabInitialization : IInitializableModule
    {
        private bool eventAttached;

        public void Initialize(InitializationEngine context)
        {
            if (eventAttached)
            {
                return;
            }

            eventAttached = true;

            RegisterTabs();
        }

        public void Uninitialize(InitializationEngine context)
        {
            eventAttached = false;
        }

        private static void RegisterTabs()
        {
            var tabDefinitionRepository = ServiceLocator.Current.GetInstance<ITabDefinitionRepository>();

            AddTabToList(tabDefinitionRepository, new TabDefinition { Name = TabNames.Content, RequiredAccess = AccessLevel.Edit, SortIndex = 1000 });
            AddTabToList(tabDefinitionRepository, new TabDefinition { Name = TabNames.Conditions, RequiredAccess = AccessLevel.Edit, SortIndex = 1500 });
            AddTabToList(tabDefinitionRepository, new TabDefinition { Name = TabNames.MoreContent, RequiredAccess = AccessLevel.Edit, SortIndex = 2000 });
            AddTabToList(tabDefinitionRepository, new TabDefinition { Name = TabNames.BasicSettings, RequiredAccess = AccessLevel.Edit, SortIndex = 3000 });
            AddTabToList(tabDefinitionRepository, new TabDefinition { Name = TabNames.SiteSettings, RequiredAccess = AccessLevel.Edit, SortIndex = 3100 });
            AddTabToList(tabDefinitionRepository, new TabDefinition { Name = TabNames.PageSettings, RequiredAccess = AccessLevel.Edit, SortIndex = 3200 });
            AddTabToList(tabDefinitionRepository, new TabDefinition { Name = SystemTabNames.Settings, RequiredAccess = AccessLevel.Edit, SortIndex = 3201 });
            AddTabToList(tabDefinitionRepository, new TabDefinition { Name = TabNames.MetaData, RequiredAccess = AccessLevel.Edit, SortIndex = 4000 });
            AddTabToList(tabDefinitionRepository, new TabDefinition { Name = TabNames.Unknown, RequiredAccess = AccessLevel.Edit, SortIndex = 5000 });
        }

        private static void AddTabToList(ITabDefinitionRepository tabDefinitionRepository, TabDefinition definition)
        {
            var existingTab = GetExistingTabDefinition(tabDefinitionRepository, definition);

            if (existingTab != null)
            {
                definition.ID = existingTab.ID;
            }

            tabDefinitionRepository.Save(definition);
        }

        private static TabDefinition GetExistingTabDefinition(ITabDefinitionRepository tabDefinitionRepository, TabDefinition definition)
        {
            return tabDefinitionRepository.List().FirstOrDefault(t => t.Name.Equals(definition.Name, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}