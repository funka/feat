﻿using System.Web.Mvc;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc.Html;
using Furb.Main.Lib.ContentArea;
using Furb.Main.Lib.Epi.Registries;
using StructureMap;

namespace Furb.Main.Initialization
{
    [InitializableModule]
    [ModuleDependency(typeof(EPiServer.Web.InitializationModule))]
    public class DependencyResolverInitialization : IConfigurableModule
    {
        public void ConfigureContainer(ServiceConfigurationContext context)
        {
            context.StructureMap().Configure(ConfigureContainer);

            DependencyResolver.SetResolver(new StructureMapDependencyResolver(context.StructureMap()));
        }

        private static void ConfigureContainer(ConfigurationExpression container)
        {
            container.AddRegistry<ScanningRegistry>();
            container.For<ContentAreaRenderer>().Use<Renderer>();
        }

        public void Initialize(InitializationEngine context)
        {
        }

        public void Uninitialize(InitializationEngine context)
        {
        }
    }
}
