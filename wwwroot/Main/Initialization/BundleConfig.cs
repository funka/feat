﻿using System.Collections.Generic;
using System.Web.Optimization;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using InitializationModule = EPiServer.Web.InitializationModule;

namespace Furb.Main.Initialization
{
    [InitializableModule]
    [ModuleDependency(typeof(InitializationModule))]
    public class BundleConfig : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            RegisterBundles(BundleTable.Bundles);
        }

        public void Uninitialize(InitializationEngine context)
        {
        }

        public static void RegisterBundles(BundleCollection bundles)
        {
            var scriptList = new List<string>
            {
                "~/Static/js/slimmage.js",

                "~/Static/js/modernizr/modernizr.js",
                "~/Static/js/jquery/jquery.js",
                "~/Static/js/jquery.ajax.unobtrusive/jquery.unobtrusive-ajax.js",
                "~/Static/js/jquery.form/jquery.form.js",

                "~/Static/js/funka/components/focusable.js",
                "~/Static/js/foundation/components/foundation.js",
                "~/Static/js/funka/components/clickable-element.js",
                "~/Static/js/funka/components/svg-fallback.js"
            };

            var deferredScriptList = new List<string>
            {
                "~/Static/js/js.cookie/js.cookie.js",
                "~/Static/js/funka/components/focusable.js",
                "~/Static/js/FunkaJS/aria.toggle.js",
                "~/Static/js/FunkaJS/tab.focus.js",
                "~/Static/js/funka/site.js",
                "~/Static/js/funka/components/login.js",
                "~/Static/js/jquery.matchHeight/jquery.matchHeight.js",
                "~/Static/js/funka/components/mobilemenu.js",
                "~/Static/js/jquery.validate/jquery.validate.js",
                "~/Static/js/FunkaJs/validation.defaults.js",
                "~/Static/js/jquery.validate.unobtrusive/jquery.validate.unobtrusive.js",
                "~/Static/js/FunkaJs/validation.unobtrusive.override.js"
            };

            var cssList = new List<string>
            {
                "~/Static/css/foundation.css",
                "~/Static/css/jquery-ui.css",
                "~/Static/css/jquery-ui.structure.css"
            };

            var noScriptCssList = new List<string>
            {
                "~/Static/css/noscript.css"
            };

            bundles.Add(new ScriptBundle("~/Static/js/furb").Include(scriptList.ToArray()));
            bundles.Add(new ScriptBundle("~/Static/js/furbDeferred").Include(deferredScriptList.ToArray()));
            bundles.Add(new StyleBundle("~/Static/css/furb").Include(cssList.ToArray()).Include("~/Static/css/funka/fonts/fonts.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Static/css/noscript").Include(noScriptCssList.ToArray()));
        }
    }
}