﻿using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using Furb.Features.Pages.Settings;
using Furb.Features.Pages.Start;

namespace Furb.Main
{
    public class ContentReferenceExtended : ContentReference
    {
        private static StartPage startPageData;

        private static SettingsPage settingsPage;

        private static ContentReference settingsPageReference;

        public static ContentReference SettingsPageReference
        {
            get
            {
                if (settingsPageReference != null)
                {
                    return settingsPageReference;
                }

                if (StartPageData == null || StartPageData.SettingsPage == null)
                {
                    return EmptyReference;
                }

                settingsPageReference = StartPageData.SettingsPage;

                return settingsPageReference;
            }
        }

        public static SettingsPage SettingsPage
        {
            get
            {
                if (settingsPage != null)
                {
                    return settingsPage;
                }

                if (SettingsPageReference == null || SettingsPageReference.ID == 0)
                {
                    return null;
                }

                var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();
                settingsPage = contentLoader.Get<SettingsPage>(SettingsPageReference);

                return settingsPage;
            }
        }

        public static StartPage StartPageData
        {
            get
            {
                if (StartPage == null || StartPage.ID == 0)
                {
                    return null;
                }

                var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();
                startPageData = contentLoader.Get<StartPage>(StartPage);

                return startPageData;
            }
        }
    }
}