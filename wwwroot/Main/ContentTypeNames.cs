﻿namespace Furb.Main
{
    public static class ContentTypeNames
    {
        public const string Content = "Content";
        public const string Specialized = "Specialized";
        public const string Local = "Local";
    }
}