﻿<?xml version="1.0" encoding="utf-8"?>

<configuration>
	<configSections>
		<section name="episerver" type="EPiServer.Configuration.EPiServerSection, EPiServer.Configuration" />
		<section name="episerver.framework" type="EPiServer.Framework.Configuration.EPiServerFrameworkSection, EPiServer.Framework.AspNet" restartOnExternalChanges="true" />
		<section name="episerver.baseLibrary" allowDefinition="MachineToApplication" allowLocation="false" type="EPiServer.BaseLibrary.ConfigurationHandler,EPiServer.BaseLibrary" />
		<section name="episerver.shell" type="EPiServer.Shell.Configuration.EPiServerShellSection, EPiServer.Shell" />
		<section name="episerver.packaging" type="EPiServer.Packaging.Configuration.EPiServerPackagingSection, EPiServer.Packaging" />
		<sectionGroup name="elmah">
			<section name="security" requirePermission="false" type="Elmah.SecuritySectionHandler, Elmah" />
			<section name="errorLog" requirePermission="false" type="Elmah.ErrorLogSectionHandler, Elmah" />
			<section name="errorMail" requirePermission="false" type="Elmah.ErrorMailSectionHandler, Elmah" />
			<section name="errorFilter" requirePermission="false" type="Elmah.ErrorFilterSectionHandler, Elmah" />
		</sectionGroup>
		<section name="resizer" type="ImageResizer.ResizerSection" requirePermission="false" />
		<section name="entityFramework" type="System.Data.Entity.Internal.ConfigFile.EntityFrameworkSection, EntityFramework, Version=6.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false" />
	</configSections>
	<connectionStrings>
		<remove name="EPiServerDB" />

		<!--Local Database-->
		<add name="EPiServerDB" connectionString="Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=Feat;Integrated Security=SSPI;AttachDBFilename=|DataDirectory|\dbFeat.mdf;MultipleActiveResultSets=True" providerName="System.Data.SqlClient" />

		<!--SQL Database-->
		<!--<add name="EPiServerDB" connectionString="Data Source=(local);Initial Catalog=Feat;Integrated Security=False;User ID=dbUserFeat;Password=8fcGc5E7gcdFHR36Mk8r;Connect Timeout=10;MultipleActiveResultSets=True" providerName="System.Data.SqlClient" />-->
	</connectionStrings>
	<system.net>
		<mailSettings>
			<smtp deliveryMethod="SpecifiedPickupDirectory" from="info@feat.local">
				<specifiedPickupDirectory pickupDirectoryLocation="C:\Temp\Mail\Feat" />
				<network defaultCredentials="false" />
			</smtp>
		</mailSettings>
	</system.net>
	<episerver>
		<applicationSettings globalErrorHandling="RemoteOnly" disableVersionDeletion="false" httpCacheability="Public" uiEditorCssPaths="~/Static/css/editor.css" urlRebaseKind="ToRootRelative" pageUseBrowserLanguagePreferences="false" uiShowGlobalizationUserInterface="true" subscriptionHandler="EPiServer.Personalization.SubscriptionMail,EPiServer" uiMaxVersions="10" pageValidateTemplate="false" uiUrl="~/GUI/CMS/" />
	</episerver>
	<episerver.framework updateDatabaseSchema="true">
		<appData basePath="App_Data" />
		<scanAssembly forceBinFolderScan="true" />
		<virtualRoles addClaims="true">
			<providers>
				<add name="Administrators" type="EPiServer.Security.WindowsAdministratorsRole, EPiServer.Framework" />
				<add name="Everyone" type="EPiServer.Security.EveryoneRole, EPiServer.Framework" />
				<add name="Authenticated" type="EPiServer.Security.AuthenticatedRole, EPiServer.Framework" />
				<add name="Anonymous" type="EPiServer.Security.AnonymousRole, EPiServer.Framework" />
				<add name="CmsAdmins" type="EPiServer.Security.MappedRole, EPiServer.Framework" roles="WebAdmins, Administrators" mode="Any" />
				<add name="CmsEditors" type="EPiServer.Security.MappedRole, EPiServer.Framework" roles="WebEditors" mode="Any" />
				<add name="Creator" type="EPiServer.Security.CreatorRole, EPiServer" />
				<add name="PackagingAdmins" type="EPiServer.Security.MappedRole, EPiServer.Framework" roles="WebAdmins, Administrators" mode="Any" />
			</providers>
		</virtualRoles>
		<virtualPathProviders>
			<clear />
			<add name="ProtectedModules" virtualPath="~/GUI/" physicalPath="modules\_protected" type="EPiServer.Web.Hosting.VirtualPathNonUnifiedProvider, EPiServer.Framework.AspNet" />
		</virtualPathProviders>
		<geolocation defaultProvider="maxmind">
			<providers>
				<add name="maxmind" type="EPiServer.Personalization.Providers.MaxMind.GeolocationProvider, EPiServer.ApplicationModules" databaseFileName="App_Data\GeoLiteCity.dat" />
			</providers>
		</geolocation>
		<localization fallbackBehavior="Echo, MissingMessage, FallbackCulture" fallbackCulture="en">
			<providers>
				<add virtualPath="~/LanguageFiles/Blocks" name="BlocksLangFiles" type="EPiServer.Framework.Localization.XmlResources.FileXmlLocalizationProvider, EPiServer.Framework.AspNet" />
				<add virtualPath="~/LanguageFiles/LocalBlocks" name="LocalBlocksLangFiles" type="EPiServer.Framework.Localization.XmlResources.FileXmlLocalizationProvider, EPiServer.Framework.AspNet" />
				<add virtualPath="~/LanguageFiles/Pages" name="PagesLangFiles" type="EPiServer.Framework.Localization.XmlResources.FileXmlLocalizationProvider, EPiServer.Framework.AspNet" />
				<add virtualPath="~/LanguageFiles/Other" name="OtherLangFiles" type="EPiServer.Framework.Localization.XmlResources.FileXmlLocalizationProvider, EPiServer.Framework.AspNet" />
				<add virtualPath="~/LanguageFiles/Forms" name="FormsFiles" type="EPiServer.Framework.Localization.XmlResources.FileXmlLocalizationProvider, EPiServer.Framework.AspNet" />
			</providers>
		</localization>
		<blob defaultProvider="sqlBlobProvider">
			<providers>
				<add name="sqlBlobProvider" type="EPiCode.SqlBlobProvider.SqlBlobProvider, EPiCode.SqlBlobProvider" />
			</providers>
		</blob>
	</episerver.framework>
	<appSettings>
		<add key="webpages:Version" value="3.0.0.0" />
		<add key="webpages:Enabled" value="false" />
		<add key="PreserveLoginUrl" value="true" />
		<add key="ClientValidationEnabled" value="true" />
		<add key="UnobtrusiveJavaScriptEnabled" value="true" />
		<add key="elmah.mvc.disableHandler" value="false" />
		<add key="elmah.mvc.disableHandleErrorFilter" value="false" />
		<add key="elmah.mvc.requiresAuthentication" value="false" />
		<add key="elmah.mvc.IgnoreDefaultRoute" value="false" />
		<add key="elmah.mvc.allowedRoles" value="*" />
		<add key="elmah.mvc.allowedUsers" value="*" />
		<add key="elmah.mvc.route" value="elmah" />
		<add key="elmah.mvc.UserAuthCaseSensitive" value="true" />
		<add key="DatabaseBackupPath" value="False" />
		<add key="aspnet:UseTaskFriendlySynchronizationContext" value="true" />
		<add key="Epi.WebSockets.Enabled" value="false" />
	</appSettings>
	<system.web>
		<compilation debug="true" targetFramework="4.7.2" optimizeCompilations="true">
			<assemblies>
				<add assembly="System.Core, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089" />
			</assemblies>
		</compilation>
		<pages validateRequest="false" enableEventValidation="true" pageParserFilterType="System.Web.Mvc.ViewTypeParserFilter, System.Web.Mvc, Version=5.2.3.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" enableViewState="false">
			<namespaces>
				<add namespace="System.Web.Helpers" />
				<add namespace="System.Web.Mvc" />
				<add namespace="System.Web.Mvc.Ajax" />
				<add namespace="System.Web.Mvc.Html" />
				<add namespace="System.Web.Routing" />
				<add namespace="System.Web.WebPages" />
				<add namespace="EPiServer.Framework.Web.Mvc.Html" />
				<add namespace="Furb.Features.Pages" />
				<add namespace="Furb.Features.LocalBlocks" />
				<add namespace="EPiServer.Shell.Web.Mvc.Html" />
			</namespaces>
			<controls>
				<add tagPrefix="EPiServer" namespace="EPiServer.Web.WebControls" assembly="EPiServer.Cms.AspNet" />
				<add tagPrefix="EPiServer" namespace="EPiServer.Web.WebControls" assembly="EPiServer.Web.WebControls" />
				<add tagPrefix="EPiServer" namespace="EPiServer.Framework.Web.WebControls" assembly="EPiServer.Framework.AspNet" />
			</controls>
		</pages>
		<globalization culture="en-US" uiCulture="en" requestEncoding="utf-8" responseEncoding="utf-8" resourceProviderFactoryType="EPiServer.Framework.Localization.LocalizationServiceResourceProviderFactory, EPiServer.Framework.AspNet" />
		<httpRuntime requestValidationMode="2.0" />
		<caching>
			<outputCacheSettings>
				<outputCacheProfiles>
					<add name="ClientResourceCache" enabled="true" duration="3600" varyByParam="*" varyByContentEncoding="gzip;deflate" />
				</outputCacheProfiles>
			</outputCacheSettings>
		</caching>
		<authentication mode="Forms">
			<forms name=".EPiServerLogin" loginUrl="Util/login.aspx" timeout="120" defaultUrl="~/" />
		</authentication>
		<profile defaultProvider="DefaultProfileProvider">
			<properties>
				<add name="Address" type="System.String" />
				<add name="ZipCode" type="System.String" />
				<add name="Locality" type="System.String" />
				<add name="Email" type="System.String" />
				<add name="FirstName" type="System.String" />
				<add name="LastName" type="System.String" />
				<add name="Language" type="System.String" />
				<add name="Country" type="System.String" />
				<add name="Company" type="System.String" />
				<add name="Title" type="System.String" />
				<add name="CustomExplorerTreePanel" type="System.String" />
				<add name="FileManagerFavourites" type="System.Collections.Generic.List`1[System.String]" />
				<add name="EditTreeSettings" type="EPiServer.Personalization.GuiSettings, EPiServer.Cms.AspNet" />
				<add name="ClientToolsActivationKey" type="System.String" />
				<add name="FrameworkName" type="System.String" />
			</properties>
			<providers>
				<add name="DefaultProfileProvider" type="System.Web.Providers.DefaultProfileProvider, System.Web.Providers, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" connectionStringName="EPiServerDB" applicationName="/" />
			</providers>
		</profile>
		<membership defaultProvider="SqlServerMembershipProvider" userIsOnlineTimeWindow="10" hashAlgorithmType="HMACSHA512">
			<providers>
				<clear />
				<add name="SqlServerMembershipProvider" type="System.Web.Providers.DefaultMembershipProvider, System.Web.Providers, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" connectionStringName="EPiServerDB" enablePasswordRetrieval="false" enablePasswordReset="true" requiresQuestionAndAnswer="false" requiresUniqueEmail="false" maxInvalidPasswordAttempts="5" minRequiredPasswordLength="6" minRequiredNonalphanumericCharacters="0" passwordAttemptWindow="10" applicationName="/" />
			</providers>
		</membership>
		<roleManager enabled="true" defaultProvider="SqlServerRoleProvider" cacheRolesInCookie="true">
			<providers>
				<clear />
				<add name="SqlServerRoleProvider" type="System.Web.Providers.DefaultRoleProvider, System.Web.Providers, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" connectionStringName="EPiServerDB" applicationName="/" />
			</providers>
		</roleManager>
		<httpModules>
			<add name="ErrorLog" type="Elmah.ErrorLogModule, Elmah" />
			<add name="ErrorMail" type="Elmah.ErrorMailModule, Elmah" />
			<add name="ErrorFilter" type="Elmah.ErrorFilterModule, Elmah" />
			<add name="ImageResizingModule" type="ImageResizer.InterceptModule" />
		</httpModules>
	</system.web>
	<system.webServer>
		<security>
			<requestFiltering>
				<requestLimits maxAllowedContentLength="4294967295" />
			</requestFiltering>
		</security>
		<validation validateIntegratedModeConfiguration="false" />
		<modules runAllManagedModulesForAllRequests="true">
			<remove name="InitializationModule" />
			<remove name="UrlRewriteModule" />
			<remove name="SlimResponseModule" />
			<remove name="ErrorLog" />
			<remove name="ErrorMail" />
			<remove name="ErrorFilter" />
			<remove name="ImageResizingModule" />
			<remove name="ShellRoutingModule" />
			<add name="InitializationModule" type="EPiServer.Framework.Initialization.InitializationModule, EPiServer.Framework.AspNet" preCondition="managedHandler" />
			<add name="UrlRewriteModule" type="EPiServer.Web.RoutingUrlRewriteModule, EPiServer.Cms.AspNet" preCondition="managedHandler" />
			<add name="SlimResponseModule" type="Imazen.SlimResponse.SlimResponseModule, Imazen.SlimResponse" />
			<add name="ErrorLog" type="Elmah.ErrorLogModule, Elmah" preCondition="managedHandler" />
			<add name="ErrorMail" type="Elmah.ErrorMailModule, Elmah" preCondition="managedHandler" />
			<add name="ErrorFilter" type="Elmah.ErrorFilterModule, Elmah" preCondition="managedHandler" />
			<add name="ImageResizingModule" type="ImageResizer.InterceptModule" />
			<add name="ShellRoutingModule" type="EPiServer.Shell.Web.Routing.ShellRoutingModule, EPiServer.Shell" />
		</modules>
		<handlers>
			<remove name="ExtensionlessUrlHandler-ISAPI-4.0_32bit" />
			<remove name="ExtensionlessUrlHandler-ISAPI-4.0_64bit" />
			<remove name="ExtensionlessUrlHandler-Integrated-4.0" />
			<remove name="OPTIONSVerbHandler" />
			<remove name="TRACEVerbHandler" />
			<remove name="UrlRoutingHandler" />
			<remove name="Elmah" />
			<remove name="ExtensionlessUrlHandler-Integrated-4.0" />
			<add name="UrlRoutingHandler" preCondition="integratedMode" verb="*" path="UrlRouting.axd" type="System.Web.HttpForbiddenHandler, System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" />
			<add name="Elmah" path="elmah.axd" verb="POST,GET,HEAD" type="Elmah.ErrorLogPageFactory, Elmah" preCondition="integratedMode" />
			<add name="ExtensionlessUrlHandler-Integrated-4.0" path="*." verb="*" type="System.Web.Handlers.TransferRequestHandler" preCondition="integratedMode,runtimeVersionv4.0" />
		</handlers>
		<staticContent>
			<clientCache cacheControlMode="UseMaxAge" cacheControlMaxAge="1.00:00:00" />
		</staticContent>
		<caching>
			<profiles>
				<remove extension=".gif" />
				<remove extension=".png" />
				<remove extension=".js" />
				<remove extension=".css" />
				<remove extension=".jpg" />
				<remove extension=".jpeg" />
				<add extension=".gif" policy="DontCache" kernelCachePolicy="CacheUntilChange" duration="0.00:01:00" location="Any" />
				<add extension=".png" policy="DontCache" kernelCachePolicy="CacheUntilChange" duration="0.00:01:00" location="Any" />
				<add extension=".js" policy="DontCache" kernelCachePolicy="CacheUntilChange" duration="0.00:01:00" location="Any" />
				<add extension=".css" policy="DontCache" kernelCachePolicy="CacheUntilChange" duration="0.00:01:00" location="Any" />
				<add extension=".jpg" policy="DontCache" kernelCachePolicy="CacheUntilChange" duration="0.00:01:00" location="Any" />
				<add extension=".jpeg" policy="DontCache" kernelCachePolicy="CacheUntilChange" duration="0.00:01:00" location="Any" />
			</profiles>
		</caching>
	</system.webServer>
	<entityFramework>
		<defaultConnectionFactory type="System.Data.Entity.Infrastructure.SqlConnectionFactory, EntityFramework" />
		<providers>
			<provider invariantName="System.Data.SqlClient" type="System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer" />
		</providers>
	</entityFramework>
	<runtime>
		<assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.ApplicationModules" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.20.6.0" newVersion="11.20.6.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.Data.Cache" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.20.6.0" newVersion="11.20.6.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.Data" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.20.6.0" newVersion="11.20.6.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.Events" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.20.6.0" newVersion="11.20.6.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.Framework" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.20.6.0" newVersion="11.20.6.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.Licensing" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.20.6.0" newVersion="11.20.6.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.Shell" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.35.2.0" newVersion="11.35.2.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.Configuration" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.20.6.0" newVersion="11.20.6.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.20.6.0" newVersion="11.20.6.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.Enterprise" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.20.6.0" newVersion="11.20.6.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.ImageLibrary" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.20.6.0" newVersion="11.20.6.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.LinkAnalyzer" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.20.6.0" newVersion="11.20.6.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.Web.WebControls" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.20.6.0" newVersion="11.20.6.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.XForms" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-1.0.3.0" newVersion="1.0.3.0" />
			</dependentAssembly>
			<probing privatePath="modulesbin" />
			<dependentAssembly>
				<assemblyIdentity name="CMS" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-3.1.0.0" newVersion="3.1.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.Cms.Shell.UI" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.35.2.0" newVersion="11.35.2.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.Shell.UI" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.35.2.0" newVersion="11.35.2.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.UI" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.35.2.0" newVersion="11.35.2.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="Shell" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-3.1.0.0" newVersion="3.1.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.Packaging.CmdAPI" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-1.0.0.0" newVersion="1.0.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.Packaging" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-3.4.0.0" newVersion="3.4.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.Logging.Log4Net" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-2.2.2.0" newVersion="2.2.2.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-12.0.0.0" newVersion="12.0.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="WebGrease" publicKeyToken="31bf3856ad364e35" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-1.6.5135.21930" newVersion="1.6.5135.21930" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Web.Razor" publicKeyToken="31bf3856ad364e35" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Web.WebPages.Razor" publicKeyToken="31bf3856ad364e35" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Net.Http.Formatting" publicKeyToken="31bf3856ad364e35" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Web.Http" publicKeyToken="31bf3856ad364e35" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-5.2.7.0" newVersion="5.2.7.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="Castle.Core" publicKeyToken="407dd0808d44fbdc" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="Castle.Windsor" publicKeyToken="407dd0808d44fbdc" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-3.3.0.0" newVersion="3.3.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="StructureMap" publicKeyToken="e60ad81abae3c223" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-3.1.9.463" newVersion="3.1.9.463" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="Antlr3.Runtime" publicKeyToken="eb42632606e9261f" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-3.5.0.2" newVersion="3.5.0.2" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="log4net" publicKeyToken="669e0ddf0bb1aa2a" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-2.0.12.0" newVersion="2.0.12.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="HtmlAgilityPack" publicKeyToken="bd319b19eaf3b43a" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-1.11.26.0" newVersion="1.11.26.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EFCache" publicKeyToken="46c4868af4307d2c" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-1.3.3.0" newVersion="1.3.3.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.Framework.AspNet" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.20.6.0" newVersion="11.20.6.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.Cms.AspNet" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-11.20.6.0" newVersion="11.20.6.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.Cms.TinyMce" publicKeyToken="8fe83dea738b45b7" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-2.13.4.0" newVersion="2.13.4.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="EPiServer.ServiceLocation.StructureMap" publicKeyToken="null" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-2.0.3.0" newVersion="2.0.3.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.ValueTuple" publicKeyToken="cc7b13ffcd2ddd51" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-4.0.3.0" newVersion="4.0.3.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Web.Helpers" publicKeyToken="31bf3856ad364e35" />
				<bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Web.WebPages" publicKeyToken="31bf3856ad364e35" />
				<bindingRedirect oldVersion="0.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35" />
				<bindingRedirect oldVersion="0.0.0.0-5.2.7.0" newVersion="5.2.7.0" />
			</dependentAssembly>
		</assemblyBinding>
	</runtime>
	<episerver.shell>
		<publicModules rootPath="~/modules/" autoDiscovery="Modules" />
		<protectedModules rootPath="~/GUI/">
			<add name="EPiServer.Packaging.UI" />
			<add name="createnewproperties" />
			<add name="hidepropertiesandtabs" />
			<add name="Shell" />
			<add name="CMS" />
			<add name="EPiServer.Cms.TinyMce" />
			<add name="EPiServer.Forms.UI" />
			<add name="EPiServer.Forms" />
		</protectedModules>
	</episerver.shell>
	<location path="GUI">
		<system.web>
			<httpRuntime maxRequestLength="4000000" requestValidationMode="2.0" />
			<pages enableEventValidation="true" enableViewState="true" enableSessionState="true" enableViewStateMac="true">
				<controls>
					<add tagPrefix="EPiServerUI" namespace="EPiServer.UI.WebControls" assembly="EPiServer.UI" />
					<add tagPrefix="EPiServerScript" namespace="EPiServer.ClientScript.WebControls" assembly="EPiServer.Cms.AspNet" />
					<add tagPrefix="EPiServerScript" namespace="EPiServer.UI.ClientScript.WebControls" assembly="EPiServer.UI" />
				</controls>
			</pages>
			<globalization requestEncoding="utf-8" responseEncoding="utf-8" />
			<authorization>
				<allow roles="WebEditors, WebAdmins, Administrators" />
				<deny users="*" />
			</authorization>
		</system.web>
		<system.webServer>
			<handlers>
				<clear />
				<add name="AssemblyResourceLoader-Integrated-4.0" path="WebResource.axd" verb="GET,DEBUG" type="System.Web.Handlers.AssemblyResourceLoader" preCondition="integratedMode,runtimeVersionv4.0" />
				<add name="PageHandlerFactory-Integrated-4.0" path="*.aspx" verb="GET,HEAD,POST,DEBUG" type="System.Web.UI.PageHandlerFactory" preCondition="integratedMode,runtimeVersionv4.0" />
				<add name="SimpleHandlerFactory-Integrated-4.0" path="*.ashx" verb="GET,HEAD,POST,DEBUG" type="System.Web.UI.SimpleHandlerFactory" preCondition="integratedMode,runtimeVersionv4.0" />
				<add name="WebServiceHandlerFactory-Integrated-4.0" path="*.asmx" verb="GET,HEAD,POST,DEBUG" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" preCondition="integratedMode,runtimeVersionv4.0" />
				<add name="svc-Integrated-4.0" path="*.svc" verb="*" type="System.ServiceModel.Activation.ServiceHttpHandlerFactory, System.ServiceModel.Activation, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" preCondition="integratedMode,runtimeVersionv4.0" />
				<add name="wildcard" path="*" verb="*" type="EPiServer.Web.StaticFileHandler, EPiServer.Framework.AspNet" />
			</handlers>
		</system.webServer>
	</location>
	<location path="GUI/CMS/admin">
		<system.web>
			<authorization>
				<allow roles="WebAdmins, Administrators" />
				<deny users="*" />
			</authorization>
		</system.web>
	</location>
	<location path="util">
		<system.web>
			<pages enableEventValidation="true">
				<controls>
					<add tagPrefix="EPiServerUI" namespace="EPiServer.UI.WebControls" assembly="EPiServer.UI" />
					<add tagPrefix="EPiServerScript" namespace="EPiServer.ClientScript.WebControls" assembly="EPiServer.Cms.AspNet" />
				</controls>
			</pages>
			<globalization requestEncoding="utf-8" responseEncoding="utf-8" />
		</system.web>
		<system.webServer>
			<handlers>
				<clear />
				<add name="AssemblyResourceLoader-Integrated-4.0" path="WebResource.axd" verb="GET,DEBUG" type="System.Web.Handlers.AssemblyResourceLoader" preCondition="integratedMode,runtimeVersionv4.0" />
				<add name="PageHandlerFactory-Integrated-4.0" path="*.aspx" verb="GET,HEAD,POST,DEBUG" type="System.Web.UI.PageHandlerFactory" preCondition="integratedMode,runtimeVersionv4.0" />
				<add name="SimpleHandlerFactory-Integrated-4.0" path="*.ashx" verb="GET,HEAD,POST,DEBUG" type="System.Web.UI.SimpleHandlerFactory" preCondition="integratedMode,runtimeVersionv4.0" />
				<add name="WebServiceHandlerFactory-Integrated-4.0" path="*.asmx" verb="GET,HEAD,POST,DEBUG" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" preCondition="integratedMode,runtimeVersionv4.0" />
				<add name="wildcard" path="*" verb="*" type="EPiServer.Web.StaticFileHandler, EPiServer.Framework.AspNet" />
			</handlers>
		</system.webServer>
	</location>
	<location path="modulesbin">
		<system.web>
			<authorization>
				<deny users="*" />
			</authorization>
		</system.web>
	</location>
	<episerver.packaging protectedVirtualPath="~/GUI/" protectedPath="modules/_Protected" publicVirtualPath="~/modules/" publicPath="modules" />
	<location path="Modules/_Protected">
		<system.webServer>
			<validation validateIntegratedModeConfiguration="false" />
			<handlers>
				<clear />
				<add name="BlockDirectAccessToProtectedModules" path="*" verb="*" preCondition="integratedMode" type="System.Web.HttpNotFoundHandler" />
			</handlers>
		</system.webServer>
	</location>
	<resizer>
		<plugins>
			<add name="DiskCache" />
			<add name="EPiServerBlobReaderPlugin" />
		</plugins>
	</resizer>
	<elmah>
		<security allowRemoteAccess="yes" />
		<errorLog type="Elmah.XmlFileErrorLog, Elmah" logPath="App_Data\ElmahLogFiles" />
	</elmah>
	<location path="elmah.axd" inheritInChildApplications="false">
		<system.web>
			<authorization>
				<allow roles="Administrator, Administrators" />
				<deny users="*" />
			</authorization>
			<httpHandlers>
				<add verb="POST,GET,HEAD" path="elmah.axd" type="Elmah.ErrorLogPageFactory, Elmah" />
			</httpHandlers>
		</system.web>
		<system.webServer>
			<handlers>
				<add name="ELMAH" verb="POST,GET,HEAD" path="elmah.axd" type="Elmah.ErrorLogPageFactory, Elmah" preCondition="integratedMode" />
			</handlers>
		</system.webServer>
	</location>
	<system.codedom>
		<compilers>
			<compiler extension=".cs" language="c#;cs;csharp" warningLevel="4" compilerOptions="/langversion:7.3 /nowarn:1659;1699;1701;612;618" type="Microsoft.CodeDom.Providers.DotNetCompilerPlatform.CSharpCodeProvider, Microsoft.CodeDom.Providers.DotNetCompilerPlatform, Version=3.6.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" />
			<compiler extension=".vb" language="vb;vbs;visualbasic;vbscript" warningLevel="4" compilerOptions="/langversion:default /nowarn:41008,40000,40008 /define:_MYTYPE=\&quot;Web\&quot; /optionInfer+" type="Microsoft.CodeDom.Providers.DotNetCompilerPlatform.VBCodeProvider, Microsoft.CodeDom.Providers.DotNetCompilerPlatform, Version=3.6.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" />
		</compilers>
	</system.codedom>
</configuration>