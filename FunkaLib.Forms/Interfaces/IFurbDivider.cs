﻿using EPiServer.Forms.Core.Models;

namespace FunkaLib.Forms.Interfaces
{
    public interface IFurbDivider : IFormElement
    {
        string Text { get; set; }
    }
}