﻿using EPiServer.Forms.Core.Validation;
using System.Collections.Generic;

namespace FunkaLib.Forms.Interfaces
{
    public interface IFurbElementBlockBase
    {
        string ValidationElement { get; set; }

        IEnumerable<ValidatorMessage> ValidatorMessages { get; set; }
    }
}