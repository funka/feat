﻿using EPiServer.Forms.Core.Models;
using EPiServer.Forms.EditView.Models.Internal;
using System.Collections.Generic;

namespace FunkaLib.Forms.Interfaces
{
    public interface IFurbDropDown : IFormElement
    {
        IEnumerable<OptionItem> Options { get; }
    }
}