﻿namespace FunkaLib.Forms.Interfaces
{
    internal interface IHelpText
    {
        string HelpText { get; set; }
    }
}