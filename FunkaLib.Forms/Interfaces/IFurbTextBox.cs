﻿using EPiServer.Forms.Core.Models;

namespace FunkaLib.Forms.Interfaces
{
    public interface IFurbTextBox : IFormElement
    {
        string Text { get; set; }
        string LabelText { get; set; }
    }
}