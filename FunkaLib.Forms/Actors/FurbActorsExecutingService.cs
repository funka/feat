﻿using EPiServer.Core;
using EPiServer.Forms.Core.Models;
using EPiServer.Forms.Core.PostSubmissionActor;
using EPiServer.Forms.Helpers.Internal;
using EPiServer.Forms.Implementation.Actors;
using EPiServer.Forms.Implementation.Elements;
using FunkaLib.Forms.Blocks;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Configuration;
using System.Web;

namespace FunkaLib.Forms.Actors
{
    public class FurbActorsExecutingService : ActorsExecutingService
    {
        private SmtpSection smtpSection;

        public override IEnumerable<IPostSubmissionActor> GetFormSubmissionActors(Submission submission, FormContainerBlock formContainer, FormIdentity formIden, HttpRequestBase request, HttpResponseBase response, bool isFormFinalizedSubmission)
        {
            smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

            var actors = base.GetFormSubmissionActors(submission, formContainer, formIden, request, response, isFormFinalizedSubmission).ToList();
            var emailActor = actors.OfType<SendEmailAfterSubmissionActor>().FirstOrDefault();

            var honey = formContainer.ElementsArea.Items.FirstOrDefault(x => x.GetContent() is FurbHoneypotBlock);

            if (honey != null)
            {
                var name = honey.ContentLink.GetElementName();
                var matchedKvp = submission.Data.FirstOrDefault(x => x.Key.Equals(name));
                var matchedValue = matchedKvp.Value;
                var date = DateTime.Now.ToString("yyyy-M-d");

                if ((string)matchedValue != date)
                {
                    throw new ArgumentException("bot detected");
                }
            }

            var emailModels = emailActor?.Model as IEnumerable<EmailTemplateActorModel>;

            if (emailModels != null && emailModels.Any())
            {
                foreach (var emailModel in emailModels)
                {
                    if (string.IsNullOrWhiteSpace(emailModel.FromEmail))
                    {
                        emailModel.FromEmail = !string.IsNullOrWhiteSpace(smtpSection.From) ? smtpSection.From : "noreply@funka.com";
                    }
                }
            }

            return actors;
        }
    }
}