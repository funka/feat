﻿// ReSharper disable InconsistentNaming
namespace FunkaLib.Forms.Enum
{
    public enum AutoComplete
    {
        Empty = 10,
        Off = 20,
        Other = 30,
        Name = 100,
        Given_Name = 110,
        Family_Name = 120,
        Address_Line1 = 200,
        Address_Level1 = 210,
        Postal_Code = 220,
        Tel = 300,
        Home__Tel = 310,
        Work__Tel = 320,
        Email = 400,
        Home__Email = 410,
        Work__Email = 420,
    }
}