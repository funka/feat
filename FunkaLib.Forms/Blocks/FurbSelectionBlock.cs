﻿using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Forms.Core.Models;
using EPiServer.Forms.EditView.DataAnnotations;
using EPiServer.Forms.EditView.Models.Internal;
using EPiServer.Forms.Implementation.Elements;
using EPiServer.Forms.Implementation.Elements.BaseClasses;
using EPiServer.Web;
using FunkaLib.Forms.Elements;
using FunkaLib.Forms.Interfaces;
using FunkaLib.Forms.Validators;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using FunkaLib.Forms.Rendering.ContentIcon;

namespace FunkaLib.Forms.Blocks
{
    [AvailableContentTypes(Availability = Availability.None, IncludeOn = new[] { typeof(FormContainerBlock) })]
    [ContentType(GUID = "5d5c8717-872d-45d4-b873-6b980a6a810b", GroupName = "BasicElements", Order = 1900)]
    [AvailableValidatorTypes(Include = new[] { typeof(FurbRequiredValidator) })]
    [ImageUrl("~/Static/Forms/Images/selectboxes.png")]
    [ContentIcon(ContentIcon.List)]
    public class FurbSelectionBlock : SelectionElementBlockBase<OptionItem>, IHelpText, IFurbElementBlockBase
    {
        private IFurbSelectList selectList;

        [Display(GroupName = SystemTabNames.Content, Order = 4000)]
        public override string Validators { get; set; }

        [Display(GroupName = SystemTabNames.Content, Order = 2)]
        public virtual bool HideHeader { get; set; }

        [UIHint(UIHint.Textarea)]
        [Display(GroupName = SystemTabNames.Content, Order = 7000)]
        public virtual string HelpText { get; set; }

        [Ignore]
        public override string Description { get; set; }

        [Ignore]
        public override string PlaceHolder { get; set; }

        [ScaffoldColumn(false)]
        public virtual string ValidationElement { get; set; }

        public string GetValidationCssClasses()
        {
            return _validationService.Service.GetValidationCssClasses(this);
        }

        public override IFormElement FormElement
        {
            get
            {
                if (selectList == null || IsModified)
                {
                    selectList = new FurbSelectList(Content);
                }

                return selectList;
            }
        }

        public override string EditViewFriendlyTitle
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(Label))
                {
                    return Label;
                }

                return !string.IsNullOrWhiteSpace(Content.Name) ? Content.Name : base.EditViewFriendlyTitle;
            }
        }

        public virtual bool IsRequired()
        {
            return FormElement.Validators.Any(x => x.GetType() == typeof(FurbRequiredValidator));
        }
    }
}