﻿using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Forms.Core.Models;
using EPiServer.Forms.EditView.DataAnnotations;
using EPiServer.Forms.Implementation.Elements;
using EPiServer.Forms.Implementation.Validation;
using EPiServer.ServiceLocation;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Web;
using FunkaLib.EpiCreateNewProperties.Attributes;
using FunkaLib.EpiHidePropertiesAndTabs;
using FunkaLib.Forms.Base;
using FunkaLib.Forms.Elements;
using FunkaLib.Forms.Enum;
using FunkaLib.Forms.Interfaces;
using FunkaLib.Forms.Validators;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using FunkaLib.Forms.Rendering.ContentIcon;

namespace FunkaLib.Forms.Blocks
{
    [AvailableContentTypes(Availability = Availability.None, IncludeOn = new[] { typeof(FormContainerBlock) })]
    [ContentType(GUID = "297DAC8D-32AF-4897-A65F-21636AC8D9B7", GroupName = "BasicElements", Order = 1900)]
    [AvailableValidatorTypes(Include = new[] { typeof(FurbRequiredValidator), typeof(FurbEmailValidator), typeof(DateYYYYMMDDValidator), typeof(FurbMaxCharValidator) })]
    [ServiceConfiguration(ServiceType = typeof(IFurbTextBoxBlock))]
    [ImageUrl("~/Static/Forms/Images/textfield.png")]
    [ContentIcon(ContentIcon.Rename)]
    public class FurbTextBoxBlock : FurbInputElementBlockBase, IFurbTextBoxBlock, IHelpText, IFurbElementBlockBase
    {
        private IFurbTextBox textBox;

        [Required]
        [Display(GroupName = SystemTabNames.Content, Order = 900)]
        [SelectOne(SelectionFactoryType = typeof(EnumSelectionFactory<TextBoxType>))]
        public virtual TextBoxType TypeSelect { get; set; }

        [Required]
        [RequiresLayoutRefresh]
        [Display(GroupName = SystemTabNames.Content, Order = 1000)]
        [SelectOne(SelectionFactoryType = typeof(EnumSelectionFactory<AutoComplete>))]
        public virtual AutoComplete AutoCompleteSelect { get; set; }

        [DisplayOnContentCreate]
        [Display(GroupName = SystemTabNames.Content, Order = 1005)]
        public virtual string AutoComplete { get; set; }

        [DisplayOnContentCreate]
        [Display(GroupName = SystemTabNames.Content, Order = 2000)]
        public virtual bool IsTextArea { get; set; }

        [DisplayOnContentCreate]
        [Display(GroupName = SystemTabNames.Content, Order = 4000)]
        public override string Validators { get; set; }

        [UIHint(UIHint.Textarea)]
        [Display(GroupName = SystemTabNames.Content, Order = 7000)]
        public virtual string HelpText { get; set; }

        [ScaffoldColumn(false)]
        public virtual string Text { get; set; }

        [ScaffoldColumn(false)]
        public virtual string ValidationElement { get; set; }

        public virtual string GetValidationCssClasses()
        {
            return _validationService.Service.GetValidationCssClasses(this);
        }

        public virtual bool IsRequired()
        {
            return FormElement.Validators.Any(x => x.GetType() == typeof(FurbRequiredValidator));
        }

        public override IFormElement FormElement
        {
            get
            {
                if (textBox == null || IsModified)
                {
                    textBox = new FurbTextBox(Content);
                }

                return textBox;
            }
        }
    }
}