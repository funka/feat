﻿using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Forms.Core.Models;
using EPiServer.Forms.EditView.DataAnnotations;
using EPiServer.Forms.EditView.Models.Internal;
using EPiServer.Forms.Implementation.Elements;
using EPiServer.Forms.Implementation.Elements.BaseClasses;
using EPiServer.Web;
using FunkaLib.Forms.Elements;
using FunkaLib.Forms.Interfaces;
using FunkaLib.Forms.Validators;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using FunkaLib.Forms.Rendering.ContentIcon;

namespace FunkaLib.Forms.Blocks
{
    [AvailableContentTypes(Availability.None, IncludeOn = new[] { typeof(FormContainerBlock) })]
    [ContentType(GUID = "71f14b03-ef52-4b93-b15e-62ea0956cc87", GroupName = "BasicElements", Order = 1900)]
    [AvailableValidatorTypes(Include = new[] { typeof(FurbRequiredValidator) })]
    [ImageUrl("~/Static/Forms/Images/dropdown.png")]
    [ContentIcon(ContentIcon.FormSelectionElementBlock)]
    public class FurbDropDownBlock : SelectionElementBlockBase<OptionItem>, IHelpText, IFurbElementBlockBase
    {
        private IFurbDropDown dropDown;

        [UIHint(UIHint.Textarea)]
        [Display(GroupName = SystemTabNames.Content, Order = 7000)]
        public virtual string HelpText { get; set; }

        [ScaffoldColumn(false)]
        public virtual string ValidationElement { get; set; }

        public virtual string GetValidationCssClasses()
        {
            return _validationService.Service.GetValidationCssClasses(this);
        }

        public override void SetDefaultValues(ContentType type)
        {
            base.SetDefaultValues(type);
        }

        public virtual bool IsRequired()
        {
            return FormElement.Validators.Any(x => x.GetType() == typeof(FurbRequiredValidator));
        }

        public override IFormElement FormElement
        {
            get
            {
                if (dropDown == null || IsModified)
                {
                    dropDown = new FurbDropDown(Content);
                }

                return dropDown;
            }
        }

        public override string EditViewFriendlyTitle
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(Label))
                {
                    return Label;
                }

                return !string.IsNullOrWhiteSpace(Content.Name) ? Content.Name : base.EditViewFriendlyTitle;
            }
        }
    }
}