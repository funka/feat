﻿using EPiServer.Core;
using EPiServer.Forms.Core.Models;
using FunkaLib.Forms.Blocks;
using FunkaLib.Forms.Interfaces;
using System;

namespace FunkaLib.Forms.Elements
{
    public class FurbDivider : FormElement, IFurbDivider
    {
        public FurbDivider(IContent sourceContent) : base(sourceContent)
        {
        }

        private FurbDividerBlock FurbDividerBlock
        {
            get => SourceContent as FurbDividerBlock;
            set => throw new NotSupportedException();
        }

        public string Text { get; set; }
    }
}