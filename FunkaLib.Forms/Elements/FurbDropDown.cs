﻿using EPiServer.Core;
using EPiServer.Forms.Core.Models;
using EPiServer.Forms.EditView.Models.Internal;
using EPiServer.Forms.Implementation.Elements.BaseClasses;
using FunkaLib.Forms.Interfaces;
using System.Collections.Generic;

namespace FunkaLib.Forms.Elements
{
    public class FurbDropDown : FormElement, IFurbDropDown
    {
        public FurbDropDown(IContent sourceContent) : base(sourceContent)
        {
        }

        public IEnumerable<OptionItem> Options
        {
            get
            {
                var dropDown = SourceContent as SelectionElementBlockBase<OptionItem>;
                return dropDown.GetItems();
            }
        }
    }
}