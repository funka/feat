﻿using EPiServer.Core;
using EPiServer.Forms.Core.Models;
using EPiServer.Forms.EditView.Models.Internal;
using EPiServer.Forms.Implementation.Elements.BaseClasses;
using FunkaLib.Forms.Blocks;
using FunkaLib.Forms.Interfaces;
using System;
using System.Collections.Generic;

namespace FunkaLib.Forms.Elements
{
    public class FurbSelectList : FormElement, IFurbSelectList
    {
        public FurbSelectList(IContent sourceContent) : base(sourceContent)
        {
        }

        private FurbSelectionBlock FurbSelectionBlock
        {
            get => SourceContent as FurbSelectionBlock;
            set => throw new NotSupportedException();
        }

        public IEnumerable<OptionItem> Options
        {
            get
            {
                var selectionBase = SourceContent as SelectionElementBlockBase<OptionItem>;
                return selectionBase.GetItems();
            }
            set => throw new NotImplementedException();
        }
    }
}