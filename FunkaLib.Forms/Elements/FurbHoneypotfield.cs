﻿using EPiServer.Core;
using EPiServer.Forms.Core.Models;
using FunkaLib.Forms.Blocks;
using FunkaLib.Forms.Interfaces;
using System;

namespace FunkaLib.Forms.Elements
{
    public class FurbHoneypotfield : FormElement, IFurbHoneyPotField
    {
        public FurbHoneypotfield(IContent sourceContent) : base(sourceContent)
        {
        }

        private FurbHoneypotBlock FurbHoneypotBlock
        {
            get => SourceContent as FurbHoneypotBlock;
            set => throw new NotSupportedException();
        }

        public string Text
        {
            get => FurbHoneypotBlock != null ? FurbHoneypotBlock.Text : string.Empty;

            set => throw new NotImplementedException();
        }
    }
}