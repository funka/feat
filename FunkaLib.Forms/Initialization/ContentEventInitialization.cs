﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Forms.Core;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;

namespace FunkaLib.Forms.Initialization
{
    [InitializableModule]
    public class ContentEventInitialization : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            var events = ServiceLocator.Current.GetInstance<IContentEvents>();
            events.CreatingContent += Events_CreatingContent;
        }

        public void Uninitialize(InitializationEngine context)
        {
            var events = ServiceLocator.Current.GetInstance<IContentEvents>();
            events.CreatedContent -= Events_CreatingContent;
        }

        private void Events_CreatingContent(object sender, ContentEventArgs e)
        {
            if (!(e.Content is ElementBlockBase elementBlockBase))
            {
                return;
            }

            if (e.Content.Name == "Nytt formulärelement" || e.Content.Name == "New form element")
            {
                e.Content.Name = elementBlockBase.Label;
            }
        }
    }
}