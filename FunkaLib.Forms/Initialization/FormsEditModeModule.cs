﻿using System.Web.Mvc;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using FunkaLib.Forms.Rendering;

namespace FunkaLib.Forms.Initialization
{
    [InitializableModule]
    public class FormsEditModeModule : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            ViewEngines.Engines.Add(new FormsViewEngine());
        }

        public void Uninitialize(InitializationEngine context)
        {
        }
    }
}