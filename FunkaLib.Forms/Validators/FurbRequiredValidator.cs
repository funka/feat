﻿using EPiServer.Core;
using EPiServer.Forms.Core.Models;
using EPiServer.Forms.Core.Validation;
using FunkaLib.Forms.Interfaces;
using System.Collections;
using System.Linq;

namespace FunkaLib.Forms.Validators
{
    public class FurbRequiredValidator : ElementValidatorBase
    {
        public override int ValidationOrder => 110;

        public override bool? Validate(IElementValidatable targetElement)
        {
            _validationService.Service.GetValidatorMessage(GetType(), "a");
            var submittedValue = targetElement.GetSubmittedValue();
            if (submittedValue is string value)
            {
                return !string.IsNullOrEmpty(value);
            }

            if (submittedValue is IEnumerable enumerable && !enumerable.GetEnumerator().MoveNext())
            {
                return false;
            }

            return submittedValue != null;
        }

        public override IValidationModel BuildValidationModel(IElementValidatable targetElement)
        {
            var message = "";
            var appendLastMessageChunk = false;
            var currentelementBase = GetCasted<IFurbElementBlockBase>(targetElement);

            var valList = currentelementBase.ValidatorMessages?.ToList();

            if (valList != null && valList.Any())
            {
                var furbRequiredValidator = valList.SingleOrDefault(x => x.Validator == typeof(FurbRequiredValidator).FullName);

                if (furbRequiredValidator != null)
                {
                    message = furbRequiredValidator.Message;
                }
            }

            if (string.IsNullOrEmpty(message))
            {
                // ReSharper disable once SuspiciousTypeConversion.Global
                if (currentelementBase is IContent content)
                {
                    message = content.Name;
                    appendLastMessageChunk = true;
                }
            }

            if (Model == null)
            {
                _model = new ValidationModelBase();
            }
            else
            {

                Model.Message = appendLastMessageChunk ? _validationService.Service.GetValidatorMessage(GetType()) + " " + message : message;
                Model.ValidationCssClass = "ValidationRequired";
            }

            return Model;
        }

        private static T GetCasted<T>(IElementValidatable elementValidatable)
        {
            return (T)elementValidatable;
        }
    }
}