﻿using System.Web.Mvc;

namespace FunkaLib.Forms.Rendering
{
    public class FormsViewEngine : RazorViewEngine
    {
        public FormsViewEngine()
        {
            var viewLocations = new[]
            {
                "~/Main/Views/Shared/{0}.cshtml",
                "~/Main/Views/Forms/{0}.cshtml",
                "~/Main/Views/Preview/{0}.cshtml"
            };
            PartialViewLocationFormats = viewLocations;
            ViewLocationFormats = viewLocations;
        }
    }
}