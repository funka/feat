﻿using System;

namespace FunkaLib.Forms.Rendering.ContentIcon
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ContentIconAttribute : Attribute
    {
        public ContentIcon Icon { get; set; }

        public ContentIconColor Color { get; set; }

        public ContentIconAttribute(ContentIcon icon, ContentIconColor color = ContentIconColor.Default)
        {
            Icon = icon;
            Color = color;
        }
    }
}