﻿using EPiServer.Web.Mvc;
using FunkaLib.Forms.Blocks;
using System.Web.Mvc;

namespace FunkaLib.Forms.Controllers
{
    public class DividerBlockController : BlockController<FurbDividerBlock>
    {
        public override ActionResult Index(FurbDividerBlock currentBlock)
        {
            return PartialView("FurbDivider", currentBlock);
        }
    }
}