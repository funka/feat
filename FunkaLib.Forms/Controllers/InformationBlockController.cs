﻿using EPiServer.Web.Mvc;
using FunkaLib.Forms.Blocks;
using System.Web.Mvc;

namespace FunkaLib.Forms.Controllers
{
    public class InformationBlockController : BlockController<FurbInformationBlock>
    {
        public override ActionResult Index(FurbInformationBlock currentBlock)
        {
            return PartialView("FurbInformation", currentBlock);
        }
    }
}