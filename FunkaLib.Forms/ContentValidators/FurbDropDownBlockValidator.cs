﻿using EPiServer.Core;
using EPiServer.Framework.Localization;
using EPiServer.Validation;
using FunkaLib.Forms.Blocks;
using System.Collections.Generic;
using System.Linq;

namespace FunkaLib.Forms.ContentValidators
{
    public class FurbDropDownBlockValidator : IValidate<FurbDropDownBlock>
    {
        public IEnumerable<ValidationError> Validate(FurbDropDownBlock instance)
        {
            var validationErrors = new List<ValidationError>();

            if (instance.Content.ContentLink.ID > 0)
            {
                if (!instance.Items.Any() || instance.Items.Count() < 2)
                {
                    validationErrors.Add(new ValidationError
                    {
                        ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/FurbDropDownBlock/Validation/ItemsMustHaveTwoOrMore"),
                        PropertyName = instance.GetPropertyName(x => x.Items),
                        Severity = ValidationErrorSeverity.Error,
                        ValidationType = ValidationErrorType.AttributeMatched
                    });
                }
            }

            return validationErrors;
        }
    }
}