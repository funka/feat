﻿using EPiServer.Core;
using EPiServer.Framework.Localization;
using EPiServer.Validation;
using FunkaLib.Forms.Blocks;
using FunkaLib.Forms.Enum;
using System.Collections.Generic;

namespace FunkaLib.Forms.ContentValidators
{
    public class FurbTextBoxBlockValidator : IValidate<FurbTextBoxBlock>
    {
        public IEnumerable<ValidationError> Validate(FurbTextBoxBlock instance)
        {
            var validationErrors = new List<ValidationError>();

            if (instance.AutoCompleteSelect == AutoComplete.Other && string.IsNullOrWhiteSpace(instance.AutoComplete))
            {
                validationErrors.Add(new ValidationError
                {
                    ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/FurbTextBoxBlock/Validation/AutoCompleteFreeTextEmpty"),
                    PropertyName = instance.GetPropertyName(x => x.AutoComplete),
                    Severity = ValidationErrorSeverity.Error,
                    ValidationType = ValidationErrorType.AttributeMatched
                });
            }

            return validationErrors;
        }
    }
}