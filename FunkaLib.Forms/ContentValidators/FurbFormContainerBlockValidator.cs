﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Forms.Core;
using EPiServer.Forms.Implementation.Elements;
using EPiServer.Framework.Localization;
using EPiServer.ServiceLocation;
using EPiServer.Validation;
using FunkaLib.Forms.Blocks;
using System.Collections.Generic;

namespace FunkaLib.Forms.ContentValidators
{
    public class FurbFormContainerBlockValidator : IValidate<FurbFormContainerBlock>
    {
        public IEnumerable<ValidationError> Validate(FurbFormContainerBlock instance)
        {
            var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();

            var validationErrors = new List<ValidationError>();

            if (instance.Content.ContentLink.ID > 0)
            {
                if (instance.RedirectToPage == null)
                {
                    validationErrors.Add(new ValidationError
                    {
                        ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/FurbFormContainerBlock/Validation/ThankYouPageIsNeeded"),
                        PropertyName = instance.GetPropertyName(x => x.RedirectToPage),
                        Severity = ValidationErrorSeverity.Error,
                        ValidationType = ValidationErrorType.AttributeMatched
                    });
                }

                if (instance.ElementsArea== null || instance.ElementsArea.Count == 0)
                {
                    validationErrors.Add(new ValidationError
                    {
                        ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/FurbFormContainerBlock/Validation/FieldsIsNeeded"),
                        PropertyName = instance.GetPropertyName(x => x.ElementsArea),
                        Severity = ValidationErrorSeverity.Error,
                        ValidationType = ValidationErrorType.AttributeMatched
                    });
                }
                else
                {
                    var found = false;

                    foreach (var areaItem in instance.ElementsArea.Items)
                    {
                        var elementBlock = contentLoader.Get<ElementBlockBase>(areaItem.ContentLink);

                        if (elementBlock is SubmitButtonElementBlock)
                        {
                            found = true;
                        }
                    }

                    if (!found)
                    {
                        validationErrors.Add(new ValidationError
                        {
                            ErrorMessage = LocalizationService.Current.GetString("/ContentTypes/FurbFormContainerBlock/Validation/SendButtonIsMissing"),
                            PropertyName = instance.GetPropertyName(x => x.ElementsArea),
                            Severity = ValidationErrorSeverity.Warning,
                            ValidationType = ValidationErrorType.AttributeMatched
                        });
                    }
                }
            }

            return validationErrors;
        }
    }
}