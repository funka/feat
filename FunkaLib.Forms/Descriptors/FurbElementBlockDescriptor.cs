﻿using EPiServer.Forms.Core;
using EPiServer.Shell;

namespace FunkaLib.Forms.Descriptors
{
    public class FurbElementBlockDescriptor<T> : UIDescriptor<T> where T : BlockBase
    {
        public FurbElementBlockDescriptor()
        {
            var memberInfo = GetType().BaseType;

            if (memberInfo != null)
            {
                IconClass = $"epi-forms-icon epi-forms-icon--small epi-forms-{(object) memberInfo.GetGenericArguments()[0].Name.ToLower()}__icon";
            }
        }
    }
}